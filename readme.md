Prerequisites
============

1. Visual Studio 2015 Community and Higher
2. SQL Server 2012 Express Edition or higher.
3. Windows Azure SDK and Tooling preferably the latest version.

Steps to configure
===================
1. Open the Solution in VS.
2. Compile the Projects, it would download all the required nuget packages.
3. The database connection string needs to be changed, please open the 
`web.config` under `MedicalJournals.WebPortal` project and `app.config` under the
`MedicalJournals.BackgroundJobProcessor` project and then update the connection
string based on your environment that is named as `local-server`.
4. Set the `MedicalJournals.BackgroundJobProcessor` as start-up.
5. Now open the package manager console and make sure `MedicalJournals` project
is selected in the drop-down and then run `Update-Database` command in the 
package manager console. This will setup the database.
5. And thats it.