﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("MedicalJournals.WebPortal")]
[assembly: AssemblyProduct("MedicalJournals.WebPortal")]
[assembly: CLSCompliant(true)]
[assembly: Guid("1b3dc395-b9de-46be-8e00-d9d5adf66a69")]
[assembly: NeutralResourcesLanguage("en-US")]