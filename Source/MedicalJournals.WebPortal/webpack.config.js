/// <binding AfterBuild='Run - Development' ProjectOpened='Watch - Development' />
const path = require('path');
const webpack = require('webpack');

const cleanWebpackPlugin = new (require('clean-webpack-plugin'))('./client');
const extractCss = new (require('extract-text-webpack-plugin'))('styles-[hash].css');

const config = {
    entry: {
        styles: path.join(__dirname, 'Content', 'index.scss'),
        site: path.join(__dirname, 'Scripts', 'lib', 'index.js'),
        app: path.join(__dirname, 'Scripts', 'application', 'index.js'),
        vendor: [
            'jquery',
            'jquery-validation',
            'jquery-validation-unobtrusive',
            'ms-signalr-client',
            'angular',
            'angular-animate',
            'angular-toastr'
        ]
    },
    devtool: 'eval',
    output: {
        path: './client',
        filename: '[name]-[hash].js',
        publicPath: '/client/'
    },
    module: {
        preLoaders: [
            {
                test: /\.js$/,
                loader: 'eslint-loader',
                exclude: /node_modules/
            }
        ],
        loaders: [
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                loader: extractCss.extract(['css', 'sass'])
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: {
                    presets: ['es2015'],
                    plugins: [
                        'add-module-exports'
                    ]
                }
            },
            {
                test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url?limit=10000'
            },
            {
                test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
                loader: 'file'
            }
        ]
    },
    plugins: [
        cleanWebpackPlugin,
        extractCss,
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor-[hash].js')
    ],
    eslint: {
        configFile: path.join(__dirname, 'Scripts', '.eslintrc.json')
    },
    sassLoader: {
        includePaths: [path.join(__dirname, 'node_modules')]
    }
};

if (process.env.NODE_ENV === 'production') {
    config.devtool = 'source-map';
    config.plugins.push(new webpack.optimize.UglifyJsPlugin());
}

module.exports = config;