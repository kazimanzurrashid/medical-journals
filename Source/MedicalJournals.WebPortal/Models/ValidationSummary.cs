namespace MedicalJournals.Models
{
    public class ValidationSummary
    {
        public ValidationSummary()
        {
            this.Title = "Error!";
            this.Closable = true;
            this.ExcludePropertyErrors = true;
            this.Message = "Please correct the following errors.";
        }

        public string Title { get; set; }

        public bool Closable { get; set; }

        public string Message { get; set; }

        public bool ExcludePropertyErrors { get; set; }
    }
}