﻿namespace MedicalJournals.WebPortal
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Routing;

    public static class MvcConfig
    {
        public static void Configure()
        {
            RegisterRoutes(RouteTable.Routes);
            RegisterGlobalFilters(GlobalFilters.Filters);
            RemoveUnnecessaryViewEngines(ViewEngines.Engines);
        }

        private static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new
                {
                    controller = "Home",
                    action = "Index",
                    id = UrlParameter.Optional
                });
        }

        private static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        private static void RemoveUnnecessaryViewEngines(
            ICollection<IViewEngine> viewEngines)
        {
            var unnecessaryEngines = viewEngines
                .Where(ve => ve is WebFormViewEngine).ToList();

            foreach (var viewEngine in unnecessaryEngines)
            {
                viewEngines.Remove(viewEngine);
            }
        }
    }
}