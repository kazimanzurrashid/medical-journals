namespace MedicalJournals.WebPortal
{
    using System.Linq;
    using System.Web.Http;
    using System.Web.Http.Cors;

    using Microsoft.Owin.Security.OAuth;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public static class WebApiConfig
    {
        public static void Configure(HttpConfiguration config)
        {
            RegisterFilters(config);
            EnableCors(config);
            RegisterRoutes(config);
            ConfigureSerializer(config);
            RemoveUnnecessaryFormatters(config);
        }

        private static void RegisterFilters(HttpConfiguration config)
        {
            config.SuppressHostPrincipal();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
        }

        private static void EnableCors(HttpConfiguration config)
        {
            config.SuppressHostPrincipal();

            var cors = new EnableCorsAttribute("*", "*", "*");

            config.EnableCors(cors);
        }

        private static void RegisterRoutes(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}",
                new { id = RouteParameter.Optional });
        }

        private static void ConfigureSerializer(HttpConfiguration config)
        {
            var serializerSettings = config.Formatters.JsonFormatter.SerializerSettings;
            serializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            serializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
        }

        private static void RemoveUnnecessaryFormatters(
            HttpConfiguration config)
        {
            var formatters = config.Formatters;
            var unnecessaryFormatters = formatters.Where(f => f != formatters.JsonFormatter)
                .ToList();

            foreach (var formatter in unnecessaryFormatters)
            {
                formatters.Remove(formatter);
            }
        }
    }
}