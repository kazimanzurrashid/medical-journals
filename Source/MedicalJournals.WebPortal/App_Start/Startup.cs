﻿namespace MedicalJournals.WebPortal
{
    using System;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin;
    using Microsoft.Owin.Security.Cookies;
    using Microsoft.Owin.Security.OAuth;

    using Owin;

    using DomainObjects;
    using Infrastructure;

    public class Startup
    {
        public static string PublicClientId { get; private set; }

        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            ConfigureIoC(app);
            ConfigureSignalr(app);
            ConfigureAuthentication(app);
        }

        private static void ConfigureIoC(IAppBuilder app)
        {
            app.CreatePerOwinContext(() => new DataContext());

            app.CreatePerOwinContext<UserManager<User>>(
                (option, context) => 
                IdentityFactory.CreateUserManager(context.Get<DataContext>()));
        }

        private static void ConfigureSignalr(IAppBuilder app)
        {
            app.MapSignalR();
        }

        private static void ConfigureAuthentication(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            PublicClientId = "self";

            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/token"),
                Provider = new OAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/api/sign-in"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                AllowInsecureHttp = true
            };

            app.UseOAuthBearerTokens(OAuthOptions);
        }
    }
}