namespace MedicalJournals.WebPortal
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Web.Http;
    using System.Web.Mvc;

    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Infrastructure;

    using Autofac;
    using Autofac.Integration.Mvc;
    using Autofac.Integration.SignalR;
    using Autofac.Integration.WebApi;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    using Infrastructure;

    public static class ContainerConfig
    {
        private static readonly object SyncLock = new object();
        private static volatile IContainer container;

        public static IContainer Container
        {
            get
            {
                if (container != null)
                {
                    return container;
                }

                lock (SyncLock)
                {
                    if (container == null)
                    {
                        container = Create();
                    }
                }

                return container;
            }
        }

        public static void Configure()
        {
            var local = Container;

            DependencyResolver.SetResolver(
                new Autofac.Integration.Mvc.AutofacDependencyResolver(local));

            GlobalConfiguration.Configuration.DependencyResolver =
                new AutofacWebApiDependencyResolver(local);

            GlobalHost.DependencyResolver = 
                new Autofac.Integration.SignalR.AutofacDependencyResolver(local);
        }

        private static IContainer Create()
        {
            var builder = new ContainerBuilder();
            var assemblies = AssembliesProvider.Instance.Assemblies.ToArray();

            builder.RegisterAssemblyModules(assemblies);
            RegisterMvc(builder, assemblies);
            RegisterWebApi(builder, assemblies);
            RegisterSignalr(builder, assemblies);

            var localContainer = builder.Build();

            return localContainer;
        }

        private static void RegisterMvc(
            ContainerBuilder builder,
            Assembly[] assemblies)
        {
            builder.RegisterControllers(assemblies);
            builder.RegisterModelBinderProvider();
            builder.RegisterFilterProvider();
        }

        private static void RegisterWebApi(
           ContainerBuilder builder,
           Assembly[] assemblies)
        {
            var config = GlobalConfiguration.Configuration;

            builder.RegisterWebApiFilterProvider(config);
            builder.RegisterWebApiModelBinders(assemblies);
            builder.RegisterApiControllers(assemblies);
            builder.RegisterHttpRequestMessage(config);
        }

        private static void RegisterSignalr(
            ContainerBuilder builder,
            Assembly[] assemblies)
        {
            var settings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                ContractResolver = new SignalRContractResolver()
            };

            var serializer = JsonSerializer.Create(settings);

            builder.RegisterInstance(serializer)
                .AsSelf()
                .AsImplementedInterfaces();

            builder.RegisterHubs(assemblies); 
        }

        private sealed class SignalRContractResolver : IContractResolver
        {
            private readonly Assembly signalrAssembly;
            private readonly IContractResolver camelCaseContractResolver;
            private readonly IContractResolver defaultContractResolver;

            public SignalRContractResolver()
            {
                this.defaultContractResolver = new DefaultContractResolver();
                this.camelCaseContractResolver = new CamelCasePropertyNamesContractResolver();
                this.signalrAssembly = typeof(Connection).Assembly;
            }

            public JsonContract ResolveContract(Type type)
            {
                return type.Assembly.Equals(this.signalrAssembly) ?
                    this.defaultContractResolver.ResolveContract(type) :
                    this.camelCaseContractResolver.ResolveContract(type);
            }
        }
    }
}