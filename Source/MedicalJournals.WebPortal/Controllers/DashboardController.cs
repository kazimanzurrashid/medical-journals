﻿namespace MedicalJournals.WebPortal.Controllers
{
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using Infrastructure;

    [RoutePrefix("dashboard"), Authorize]
    public class DashboardController : Controller
    {
        private readonly ICurrentUserProvider currentUserProvider;

        public DashboardController(ICurrentUserProvider currentUserProvider)
        {
            this.currentUserProvider = currentUserProvider;
        }

        [Route("")]
        public async Task<ActionResult> Index(
            CancellationToken cancellationToken)
        {
            var currentUser = await this.currentUserProvider
                .GetAsync(cancellationToken);

            this.ViewBag.IsPublisher = currentUser.IsPublisher;

            return this.View();
        }
    }
}