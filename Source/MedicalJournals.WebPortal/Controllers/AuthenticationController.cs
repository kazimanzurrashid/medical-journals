﻿namespace MedicalJournals.WebPortal.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    using Microsoft.Owin.Security.Cookies;
    using Microsoft.Owin.Security.OAuth;

    using DomainObjects;
    using Infrastructure;

    [RoutePrefix("api")]
    public class AuthenticationController : ApiController
    {
        private UserManager<User> userManager;
        private IAuthenticationManager authenticationManager;

        public virtual UserManager<User> UserManager => 
            this.userManager ??
            (this.userManager = this.Request
                .GetOwinContext()
                .GetUserManager<UserManager<User>>());

        public virtual IAuthenticationManager AuthenticationManager => 
            this.authenticationManager ?? 
            (this.authenticationManager = 
            this.Request.GetOwinContext().Authentication);

        [Route("sign-in"), HttpPost]
        public async Task<IHttpActionResult> SignIn(
            string provider,
            string error)
        {
            if (error != null)
            {
                return this.Redirect(
                    this.Url.Content("~/") + "#error=" +
                    Uri.EscapeDataString(error));
            }

            if (!this.User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            var externalSignin = ExternalSignInInfo.FromIdentity(
                this.User.Identity as ClaimsIdentity);

            if (externalSignin == null)
            {
                return this.InternalServerError();
            }

            if (externalSignin.SignInProvider != provider)
            {
                this.AuthenticationManager.SignOut(
                    DefaultAuthenticationTypes.ExternalCookie);

                return new ChallengeResult(provider, this);
            }

            var user = await this.UserManager.FindAsync(
                new UserLoginInfo(
                    externalSignin.SignInProvider,
                    externalSignin.ProviderKey));

            var exists = user != null;

            if (exists)
            {
                this.AuthenticationManager
                   .SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                var oauthIdentity = await this.UserManager
                    .CreateIdentityAsync(
                        user,
                        OAuthDefaults.AuthenticationType);

                var cookiesIdentity = await this.UserManager
                    .CreateIdentityAsync(
                        user,
                        CookieAuthenticationDefaults.AuthenticationType);

                var properties = OAuthProvider.CreateProperties(
                    user.UserName);

                this.authenticationManager.SignIn(
                    properties, 
                    oauthIdentity, 
                    cookiesIdentity);
            }
            else
            {
                var claims = externalSignin.GetClaims();
                var identity = new ClaimsIdentity(
                    claims,
                    OAuthDefaults.AuthenticationType);

                this.authenticationManager.SignIn(identity);
            }

            return this.Ok();
        }

        [Route("sign-out"), HttpPost]
        public IHttpActionResult SignOut()
        {
            this.AuthenticationManager.SignOut(
                CookieAuthenticationDefaults.AuthenticationType);

            return this.Ok();
        }

        private sealed class ExternalSignInInfo
        {
            public string SignInProvider { get; private set; }

            public string ProviderKey { get; private set; }

            private string UserName { get; set; }

            public IEnumerable<Claim> GetClaims()
            {
                var claims = new List<Claim>
                                 {
                                     new Claim(
                                         ClaimTypes.NameIdentifier,
                                         this.ProviderKey,
                                         null,
                                         this.SignInProvider)
                                 };

                if (this.UserName != null)
                {
                    claims.Add(new Claim(
                        ClaimTypes.Name, 
                        this.UserName, 
                        null,
                        this.SignInProvider));
                }

                return claims;
            }

            public static ExternalSignInInfo FromIdentity(ClaimsIdentity identity)
            {
                var providerKeyClaim = identity?.FindFirst(ClaimTypes.NameIdentifier);

                if (string.IsNullOrWhiteSpace(providerKeyClaim?.Issuer) ||
                    string.IsNullOrWhiteSpace(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalSignInInfo
                {
                    SignInProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }
    }
}