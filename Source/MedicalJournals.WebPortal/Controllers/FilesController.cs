namespace MedicalJournals.WebPortal.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    using ExtensionMethods;
    using Infrastructure;
    using Models;

    [RoutePrefix("files"), Authorize]
    public class FilesController : Controller
    {
        private readonly ICommandBus commandBus;
        private readonly ICurrentUserProvider currentUserProvider;

        public FilesController(
            ICommandBus commandBus,
            ICurrentUserProvider currentUserProvider)
        {
            this.commandBus = commandBus;
            this.currentUserProvider = currentUserProvider;
        }

        [Route(""), HttpPost]
        public async Task<ActionResult> Upload(
            HttpPostedFileBase journal)
        {
            if (journal == null)
            {
                this.ModelState
                    .AddModelError(nameof(journal), "Journal is required.");

                return new JsonNetResult
                {
                    Model = new { error = this.FirstModelStateErrorMessage() },
                    StatusCode = HttpStatusCode.BadRequest
                };
            }

            var currentUser = await this.currentUserProvider
                .GetAsync(CancellationToken.None);

            if (!currentUser.IsPublisher)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            var command = new JournalCreateCommand
            {
                Name = journal.FileName,
                Content = journal.InputStream
            };

            var result = await this.commandBus
                .SendAsync<ActionCommandResponseResult<JournalListItem>>(
                    command,
                    CancellationToken.None);

            HttpStatusCode statusCode;
            object model;

            if (result)
            {
                statusCode = HttpStatusCode.Created;
                model = result.Response;
            }
            else
            {
                result.CopyErrorsTo(this.ModelState);
                statusCode = HttpStatusCode.BadRequest;
                model = new { error = this.FirstModelStateErrorMessage() };
            }

            return new JsonNetResult
            {
                Model = model,
                StatusCode = statusCode
            };
        }

        private string FirstModelStateErrorMessage()
        {
            return this.ModelState
                .Select(x => x.Value)
                .SelectMany(v => v.Errors)
                .Select(e => e.Exception?.Message ?? e.ErrorMessage)
                .First(m => !string.IsNullOrWhiteSpace(m));
        }

        private sealed class JsonNetResult : ActionResult
        {
            private const string JsonMimeType = "application/json";

            private static readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            public object Model { private get; set; }

            public HttpStatusCode StatusCode { private get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException(nameof(context));
                }

                var response = context.HttpContext.Response;

                response.ContentType = JsonMimeType;
                response.StatusCode = (int)this.StatusCode;

                if (response.StatusCode >= (int)HttpStatusCode.BadRequest)
                {
                    response.TrySkipIisCustomErrors = true;
                }

                if (this.Model == null)
                {
                    return;
                }

                var json = JsonConvert.SerializeObject(this.Model, JsonSettings);

                if (!string.IsNullOrWhiteSpace(json))
                {
                    response.Write(json);
                }
            }
        }
    }
}