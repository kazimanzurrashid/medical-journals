﻿namespace MedicalJournals.WebPortal.Controllers
{
    using System.Collections.Generic;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http;

    using ExtensionMethods;
    using Infrastructure;
    using Models;

    [RoutePrefix("api/journals"), Authorize]
    public class JournalsController : ApiController
    {
        private readonly ICommandBus commandBus;

        public JournalsController(ICommandBus commandBus)
        {
            this.commandBus = commandBus;
        }

        [Route(""), HttpGet]
        public async Task<IHttpActionResult> All(
            [FromUri]PagedListCommand command,
            CancellationToken cancellationToken)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            var model = await this.commandBus
                .SendAsync<PagedListResult<JournalListItem>>(
                    command ?? new PagedListCommand(),
                    cancellationToken);

            return this.Ok(model);
        }

        [Route("interested"), HttpGet]
        public async Task<IHttpActionResult> Interested(
            CancellationToken cancellationToken)
        {
            var model = await this.commandBus
                .SendAsync<IEnumerable<InterestedJournalListItem>>(
                    EmptyCommand.Instance,
                    cancellationToken);

            return this.Ok(model);
        }

        [Route("{journalId}/interest"), HttpPost]
        public async Task<IHttpActionResult> Interest(
            [FromUri]InterestCreateCommand command,
            CancellationToken cancellationToken)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            var result = await this.commandBus
                .SendAsync<ActionCommandResult>(command, cancellationToken);

            if (result == null)
            {
                return this.NotFound();
            }

            if (result)
            {
                return this.StatusCode(HttpStatusCode.NoContent);
            }

            result.CopyErrorsTo(this.ModelState);

            return this.BadRequest(this.ModelState);
        }

        [Route("{journalId}/interest"), HttpDelete]
        public async Task<IHttpActionResult> Uninterest(
            [FromUri]InterestDeleteCommand command,
            CancellationToken cancellationToken)
        {
            if (!this.ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            var result = await this.commandBus
                .SendAsync<ActionCommandResult>(command, cancellationToken);

            if (result == null)
            {
                return this.NotFound();
            }

            if (result)
            {
                return this.StatusCode(HttpStatusCode.NoContent);
            }

            result.CopyErrorsTo(this.ModelState);

            return this.BadRequest(this.ModelState);
        }
    }
}