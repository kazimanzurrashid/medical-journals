﻿namespace MedicalJournals.WebPortal.Controllers
{
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using ExtensionMethods;
    using Infrastructure;
    using Models;

    [RoutePrefix("")]
    public class UserController : Controller
    {
        private readonly ICommandBus commandBus;
        private FlashMessage flash;

        public UserController(ICommandBus commandBus)
        {
            this.commandBus = commandBus;
        }

        public virtual FlashMessage Flash => 
            this.flash ?? (this.flash = new FlashMessage(this.TempData));

        [Route("register"), HttpGet]
        public ActionResult Signup()
        {
            return this.View(new SignupCommand { IsPublisher = true });
        }

        [Route("register"), HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Signup(
            SignupCommand command,
            CancellationToken cancellationToken)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(command);
            }

            var result = await this.commandBus
                .SendAsync<ActionCommandResult>(command, cancellationToken);

            if (result)
            {
                this.Flash[FlashMessageType.Success] = "Your account is " +
                    "successfully created.";

                return this.RedirectToAction("SignIn");
            }

            result.CopyErrorsTo(this.ModelState);

            return this.View(command);
        }

        [Route("sign-in"), HttpGet]
        public ActionResult SignIn()
        {
            return this.View();
        }
    }
}