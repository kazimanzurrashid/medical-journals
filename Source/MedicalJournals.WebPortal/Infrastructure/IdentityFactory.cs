namespace MedicalJournals.Infrastructure
{
    using System;
    using System.Data.Entity;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    using DomainObjects;

    public static class IdentityFactory
    {
        public static UserManager<User> CreateUserManager(
            DbContext dataContext)
        {
            var userStore = new UserStore<User>(dataContext)
                                {
                                    AutoSaveChanges = false,
                                    DisposeContext = false
                                };

            var manager = new UserManager<User>(userStore);

            manager.UserValidator = new UserValidator<User>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireDigit = false,
                RequireLowercase = false,
                RequireNonLetterOrDigit = false,
                RequireUppercase = false
            };

            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;
            manager.UserLockoutEnabledByDefault = true;

            return manager;
        }
    }
}