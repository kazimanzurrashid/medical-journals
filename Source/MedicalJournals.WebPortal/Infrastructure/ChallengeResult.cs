namespace MedicalJournals.Infrastructure
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http;

    public class ChallengeResult : IHttpActionResult
    {
        public ChallengeResult(string signInProvider, ApiController controller)
        {
            if (controller == null)
            {
                throw new ArgumentNullException(nameof(controller));
            }

            this.SignInProvider = signInProvider;
            this.Request = controller.Request;
        }

        public string SignInProvider { get; set; }

        public HttpRequestMessage Request { get; set; }

        public Task<HttpResponseMessage> ExecuteAsync(
            CancellationToken cancellationToken)
        {
            this.Request.GetOwinContext().Authentication.Challenge(
                this.SignInProvider);

            var response = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                               {
                                   RequestMessage = this.Request
                               };

            return Task.FromResult(response);
        }
    }
}