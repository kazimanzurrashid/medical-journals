namespace MedicalJournals.Infrastructure
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Web;
    using System.Web.Routing;

    using Microsoft.AspNet.Identity.Owin;

    using Autofac;

    using Destructurama;
    using Serilog;
    using SerilogWeb.Classic.Enrichers;

    using DomainObjects;
    using ExtensionMethods;
    using Models;

    public class ServiceRegistrations : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            builder.Register(c => new HttpContextWrapper(HttpContext.Current))
                .As<HttpContextBase>()
                .InstancePerRequest();

            builder.Register(c => RouteTable.Routes)
                .As<RouteCollection>()
                .SingleInstance();

            RegisterDataContext(builder);
            RegisterCurrentUserProvider(builder);
            RegisterLogger(builder);
            RegisterIdentityServices(builder);
            RegisterNewJournalUploadNotifier(builder);
        }

        private static void RegisterDataContext(ContainerBuilder builder)
        {
            var newLine = Environment.NewLine;

            builder.Register<DataContext>()
                .InstancePerRequest()
                .WithParameter("name", ConfigurationManager.AppSettings["app.database"])
                .OnActivated(
                    e =>
                    {
                        var context = e.Context.Resolve<IComponentContext>();

                        e.Instance.Database.Log = s =>
                        {
                            if (string.IsNullOrWhiteSpace(s))
                            {
                                return;
                            }

                            var arg = s;

                            while (arg.EndsWith(newLine, StringComparison.Ordinal))
                            {
                                arg = arg.Substring(0, arg.Length - newLine.Length);
                            }

                            context.Resolve<ILogger>().Debug("{EfLog}", arg);
                        };
                    });
        }

        private static void RegisterCurrentUserProvider(ContainerBuilder builder)
        {
            builder.Register<CurrentUserProvider>()
                .InstancePerRequest();
        }

        private static void RegisterIdentityServices(ContainerBuilder builder)
        {
            builder.Register(c => 
                IdentityFactory.CreateUserManager(c.Resolve<DataContext>()))
                .AsSelf()
                .AsImplementedInterfaces();

            builder.Register(c => 
                    c.Resolve<HttpContextBase>().GetOwinContext().Authentication)
                .AsSelf()
                .AsImplementedInterfaces();

            builder.Register<SignInManager<User, string>>();
        }

        private static void RegisterLogger(ContainerBuilder builder)
        {
            const string template =
                "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} " + "{HttpRequestClientHostIP} " + "{HttpRequestType} "
                + "{HttpRequestRawUrl} " + "{UserName} " + "{MachineName} " + "[{Level}] "
                + "{Message}{NewLine}{Exception}";

            var logFormat = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs") + 
                Path.DirectorySeparatorChar +
                "{Date}.txt";

            var logger =
                new LoggerConfiguration()
                    .ReadFrom
                    .AppSettings()
                    .WriteTo.RollingFile(logFormat, outputTemplate: template)
                    .Destructure.ByIgnoringProperties<SignupCommand>(c => c.Password, c => c.ConfirmPassword)
                    .Enrich.FromLogContext()
                    .Enrich.With<HttpRequestClientHostIPEnricher>()
                    .Enrich.With<HttpRequestTypeEnricher>()
                    .Enrich.With<HttpRequestRawUrlEnricher>()
                    .Enrich.With<UserNameEnricher>()
                    .Enrich.WithMachineName()
                    .CreateLogger();

            builder.RegisterInstance(logger)
                .AsSelf()
                .AsImplementedInterfaces()
                .SingleInstance();
        }

        private static void RegisterNewJournalUploadNotifier(ContainerBuilder builder)
        {
            builder.Register<NewJournalUploadNotifier>();
        }
    }
}