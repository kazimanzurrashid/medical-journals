namespace MedicalJournals.Infrastructure
{
    using System.Threading.Tasks;

    using Models;
    using WebPortal.Hubs;

    public class NewJournalUploadNotifier : INewJournalUploadNotifier
    {
        public Task Notify(JournalListItem journal)
        {
            return JournalsHub.Uploaded(journal);
        }
    }
}