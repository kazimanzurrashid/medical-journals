﻿namespace MedicalJournals.Infrastructure
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    public enum FlashMessageType
    {
        Success,
        Error,
        Info,
        Warning
    }

    public class FlashMessage : IEnumerable<KeyValuePair<string, string>>
    {
        public const string Key = "__Flash";

        private readonly IDictionary<string, object> backingStore;

        public FlashMessage(IDictionary<string, object> backingStore)
        {
            this.backingStore = backingStore;
        }

        public string this[FlashMessageType type]
        {
            get { return this[type.ToString()]; }

            set { this[type.ToString()] = value; }
        }

        public string this[string type]
        {
            get { return this.GetMessage(type); }

            set { this.SetMessage(type, value); }
        }

        public virtual IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            object messages;

            if (!this.backingStore.TryGetValue(Key, out messages))
            {
                yield break;
            }

            foreach (var item in (List<MessageItem>)messages)
            {
                yield return new KeyValuePair<string, string>(
                    item.Type, item.Message);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        protected virtual string GetMessage(string type)
        {
            object temp;

            if (!this.backingStore.TryGetValue(Key, out temp))
            {
                return null;
            }

            var messages = (List<MessageItem>)temp;
            var item = Find(type, messages);

            return item?.Message;
        }

        protected virtual void SetMessage(string type, string message)
        {
            var messages = new List<MessageItem>();
            object temp;

            if (this.backingStore.TryGetValue(Key, out temp))
            {
                messages = (List<MessageItem>)temp;
            }
            else
            {
                this.backingStore.Add(Key, messages);
            }

            var item = Find(type, messages);

            if (item == null)
            {
                if (!string.IsNullOrWhiteSpace(message))
                {
                    messages.Add(new MessageItem
                    {
                        Type = type,
                        Message = message
                    });
                }

                return;
            }

            if (message == null)
            {
                messages.Remove(item);
            }
            else
            {
                item.Message = message;
            }
        }

        private static MessageItem Find(
            string type,
            List<MessageItem> messages)
        {
            return messages.Find(m => m.Type.Equals(
                type, StringComparison.OrdinalIgnoreCase));
        }

        [Serializable]
        private sealed class MessageItem
        {
            public string Type { get; set; }

            public string Message { get; set; }
        }
    }
}