namespace MedicalJournals.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    using Microsoft.Owin.Security.Cookies;
    using Microsoft.Owin.Security.OAuth;

    using DomainObjects;

    public class OAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string publicClientId;

        public OAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException(nameof(publicClientId));
            }

            this.publicClientId = publicClientId;
        }

        public static AuthenticationProperties CreateProperties(string userName)
        {
            var data = new Dictionary<string, string>
            {
                { "userName", userName }
            };

            return new AuthenticationProperties(data);
        }

        public override async Task GrantResourceOwnerCredentials(
            OAuthGrantResourceOwnerCredentialsContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var userManager = context.OwinContext
                .GetUserManager<UserManager<User>>();

            var user = await userManager.FindAsync(
                context.UserName,
                context.Password);

            if (user == null)
            {
                context.SetError(
                    "invalid_grant",
                    "The user name or password is incorrect.");
                return;
            }

            var oauthIdentity = await userManager.CreateIdentityAsync(
                user, 
                OAuthDefaults.AuthenticationType);

            var cookiesIdentity = await userManager.CreateIdentityAsync(
                user,
                CookieAuthenticationDefaults.AuthenticationType);

            var properties = CreateProperties(user.UserName);
            var ticket = new AuthenticationTicket(oauthIdentity, properties);

            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(cookiesIdentity);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            foreach (var property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters
                    .Add(property.Key, property.Value);
            }

            return Task.FromResult(0);
        }

        public override Task ValidateClientAuthentication(
            OAuthValidateClientAuthenticationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult(0);
        }

        public override Task ValidateClientRedirectUri(
            OAuthValidateClientRedirectUriContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.ClientId != this.publicClientId)
            {
                return Task.FromResult(0);
            }

            var expectedRootUri = new Uri(
                context.Request.Uri,
                new Uri("/", UriKind.RelativeOrAbsolute));

            if (expectedRootUri.AbsoluteUri == context.RedirectUri)
            {
                context.Validated();
            }

            return Task.FromResult(0);
        }
    }
}