﻿namespace MedicalJournals.WebPortal
{
    using System;
    using System.Web;
    using System.Web.Http;

    public class MedicalJournalsApplication : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            GlobalConfiguration.Configure(WebApiConfig.Configure);
            MvcConfig.Configure();
            ContainerConfig.Configure();
        }
    }
}