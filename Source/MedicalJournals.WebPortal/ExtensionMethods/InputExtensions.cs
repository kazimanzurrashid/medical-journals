namespace MedicalJournals.ExtensionMethods
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    public static class InputExtensions
    {
        private static readonly string[] KnownAttributes =
        {
            "placeholder",
            "maxlength",
            "pattern",
            "required",
            "autocomplete",
            "autofocus",
            "readonly",
            "_readonly",
            "disabled",
            "class",
            "_class",
            "min",
            "max"
        };

        public static IDictionary<string, object> InputAttributes(
            this IViewDataContainer instance)
        {
            return InputAttributes(instance, null);
        }

        public static IDictionary<string, object> InputAttributes(
            this IViewDataContainer instance,
            object additionalAttributes)
        {
            if (instance == null)
            {
                throw new ArgumentNullException(nameof(instance));
            }

            var htmlAttributes = KnownAttributes
                .Where(attribute => instance.ViewData.ContainsKey(attribute))
                .ToDictionary(
                    attribute => attribute,
                    attribute => instance.ViewData[attribute]);

            if (!string.IsNullOrWhiteSpace(
                instance.ViewData.ModelMetadata.Watermark) &&
                !htmlAttributes.ContainsKey("placeholder"))
            {
                htmlAttributes["placeholder"] = instance.ViewData
                    .ModelMetadata.Watermark;
            }

            if (additionalAttributes != null)
            {
                var extraAttributes = additionalAttributes.ToDictionary();

                foreach (var extraAttribute in extraAttributes)
                {
                    htmlAttributes.Add(
                        extraAttribute.Key,
                        extraAttribute.Value);
                }
            }

            var cssClass = string.Empty;

            if (htmlAttributes.ContainsKey("class"))
            {
                cssClass = htmlAttributes["class"].ToString();
            }
            else if (htmlAttributes.ContainsKey("_class"))
            {
                cssClass = htmlAttributes["_class"].ToString();
            }

            if (cssClass.IndexOf(
                "form-control",
                StringComparison.Ordinal) > -1)
            {
                return htmlAttributes;
            }

            if (cssClass.Length > 0)
            {
                cssClass += " ";
            }

            cssClass += "form-control";
            htmlAttributes["class"] = cssClass;

            object readonlyValue;

            if (!htmlAttributes.TryGetValue("_readonly", out readonlyValue))
            {
                return htmlAttributes;
            }

            htmlAttributes.Remove("_readonly");
            htmlAttributes.Add("readonly", readonlyValue);

            return htmlAttributes;
        }
    }
}