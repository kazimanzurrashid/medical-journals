namespace MedicalJournals.ExtensionMethods
{
    using System;

    using Models;

    using MvcModelStateDictionary = System.Web.Mvc.ModelStateDictionary;
    using WebApiModelStateDictionary = System.Web.Http.ModelBinding.ModelStateDictionary;

    public static class ActionCommandResultExtensions
    {
        public static void CopyErrorsTo<TCommandResult>(
            this ActionCommandResultBase<TCommandResult> instance,
            MvcModelStateDictionary target) 
            where TCommandResult : ActionCommandResultBase<TCommandResult>
        {
            if (instance == null)
            {
                throw new ArgumentNullException(nameof(instance));
            }

            if (target == null)
            {
                throw new ArgumentNullException(nameof(target));
            }

            foreach (var pair in instance.AllErrors())
            {
                foreach (var message in pair.Value)
                {
                    target.AddModelError(pair.Key, message);
                }
            }
        }

        public static void CopyErrorsTo<TCommandResult>(
           this ActionCommandResultBase<TCommandResult> instance,
           WebApiModelStateDictionary target)
           where TCommandResult : ActionCommandResultBase<TCommandResult>
        {
            if (instance == null)
            {
                throw new ArgumentNullException(nameof(instance));
            }

            if (target == null)
            {
                throw new ArgumentNullException(nameof(target));
            }

            foreach (var pair in instance.AllErrors())
            {
                foreach (var message in pair.Value)
                {
                    target.AddModelError(pair.Key, message);
                }
            }
        }
    }
}