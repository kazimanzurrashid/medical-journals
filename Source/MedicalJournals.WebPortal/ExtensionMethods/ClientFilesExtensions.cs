namespace MedicalJournals.ExtensionMethods
{
    using System;
    using System.Collections.Concurrent;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public static class ClientFilesExtensions
    {
        private static readonly ConcurrentDictionary<string, string> PathMaps 
            = new ConcurrentDictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        public static string ClientDirectory { get; set; } = "client";

        public static IHtmlString StyleSheet(
            this HtmlHelper instance, 
            string name)
        {
            const string Extension = "css";

            if (instance == null)
            {
                throw new ArgumentNullException(nameof(instance));
            }

            var resolvedName = GetResolvedName(
                instance.ViewContext.HttpContext,
                name,
                Extension);

            var path = BuildPath(resolvedName, Extension);
            return instance.Raw($"<link href=\"{path}\" rel=\"stylesheet\"/>");
        }

        public static IHtmlString Script(
            this HtmlHelper instance,
            string name)
        {
            const string Extension = "js";

            if (instance == null)
            {
                throw new ArgumentNullException(nameof(instance));
            }

            var resolvedName = GetResolvedName(
                instance.ViewContext.HttpContext,
                name,
                Extension);

            var path = BuildPath(resolvedName, Extension);

            return instance.Raw($"<script src=\"{path}\"></script>");
        }

        private static string GetResolvedName(
            HttpContextBase httpContext,
            string name,
            string extension)
        {
            return httpContext.IsDebuggingEnabled ?
                ResolveName(
                    httpContext,
                    name,
                    extension) :
                PathMaps.GetOrAdd(
                name + ":" + extension,
                _ => ResolveName(
                    httpContext,
                    name,
                    extension));
        }

        private static string BuildPath(
            string name,
            string extension)
        {
            var path = ClientDirectory + 
                "/" + 
                Path.GetFileNameWithoutExtension(name) + 
                "." +
                extension;

            return path;
        }

        private static string ResolveName(
            HttpContextBase context,
            string name,
            string extension)
        {
            var localPath = context.Server.MapPath("~/" + ClientDirectory);

            var files = Directory.GetFiles(
                localPath, 
                name + "-" + "*")
                .Where(f =>
                    ("." + extension).Equals(
                        Path.GetExtension(f),
                        StringComparison.OrdinalIgnoreCase))
                .ToList();

            if (!files.Any())
            {
                throw new FileNotFoundException(
                    $"Could not find a file with prefix \"{ name }\" and " + 
                    $"extension \"{ extension }\".");
            }

            if (files.Count == 1)
            {
                return Path.GetFileName(files[0]);
            }

            return files.Select(f => new FileInfo(f))
                    .OrderByDescending(f => f.CreationTimeUtc)
                    .Select(f => f.Name)
                    .First();
        }

    }
}