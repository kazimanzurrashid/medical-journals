﻿namespace MedicalJournals.WebPortal.Hubs
{
    using System;
    using System.Threading.Tasks;

    using Microsoft.AspNet.SignalR;

    [CLSCompliant(false)]
    public class BackgroundJobNotificationsHub : Hub
    {
        public Task Push(
            string jobName,
            string eventName,
            object model)
        {
            return this.Clients.All.notify(jobName, eventName, model);
        }
    }
}