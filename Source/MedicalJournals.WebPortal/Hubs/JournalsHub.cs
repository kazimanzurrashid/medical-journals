﻿namespace MedicalJournals.WebPortal.Hubs
{
    using System;
    using System.Threading.Tasks;

    using Microsoft.AspNet.SignalR;

    using Models;

    [CLSCompliant(false)]
    public class JournalsHub : Hub
    {
        public static Task Uploaded(JournalListItem journal)
        {
            var context = GlobalHost.ConnectionManager
                .GetHubContext<JournalsHub>();

            return context.Clients.All.uploaded(journal);
        }
    }
}