﻿export default (app) => {

    function interceptor($rootScope, $q, $timeout){
        let operationCount = 0;
        let timerHandle;

        function start() {
            operationCount++;

            if (operationCount > 1) {
                return;
            }

            timerHandle = $timeout(() => {
                    $rootScope.$broadcast('mjAjaxStarted');
                },
                400);
        }

        function complete() {
            operationCount--;
            if (operationCount > 0) {
                return;
            }
            if (timerHandle) {
                $timeout.cancel(timerHandle);
                timerHandle = void 0;
            }
            $rootScope.$broadcast('mjAjaxCompleted');
        }

        return {
            request: (request) => {
                start();
                return request;
            },

            response: (response) => {
                complete();
                return response;
            },

            responseError: (reason) => {
                complete();
                return $q.reject(reason);
            }
        };
    }

    interceptor.$inject = [
        '$rootScope',
        '$q',
        '$timeout'
    ];

    function config($httpProvider) {
        $httpProvider.interceptors.unshift(interceptor);
    }

    config.$inject = [
        '$httpProvider'
    ];

    function directive() {
        const template =
            '<div class="ajax-progress text-info" data-ng-show="isInProgress">' +
                '<i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw"></i>' +
            '</div>';

        function link(scope) {
            scope.$on('mjAjaxStarted',
            () => {
                scope.isInProgress = true;
            });

            scope.$on('mjAjaxCompleted',
            () => {
                scope.isInProgress = false;
            });
        }

        return {
            scope: true,
            replace: true,
            template: template,
            link: link
        };
    }

    app.config(config).directive('mjAjaxProgress', directive);
};