﻿export default (app) => {

    const size = 25;

    class controller {

        constructor($scope, $filter, $window, toastr, server, isPublisher) {
            this._$scope = $scope;
            this._$filter = $filter;
            this._$window = $window;
            this._toastr = toastr;
            this._server = server;
            this._isPublisher = isPublisher;

            this.newJournal = void 0;

            this._sortBy = 'timestamp';
            this._descending = true;
            this._page = 0;
            this._list = void 0;
            this._total = void 0;

            this.load();

            this._$scope.$on(
                'mjFileUploaded', 
                (_, journal) => this._onUpload(journal));

            this._$scope.$on(
                'mjBackgroundJobNotification', 
                (_, notification) => this._onNotification(notification));
        }

        signout($event) {
            $event.preventDefault();
            this._server.signout()
                .then(() => {
                    this._$window.location = '/';
                });
        }

        canUpload() {
            return this._isPublisher;
        }

        upload(form, $event) {
            if (form.$invalid) {
                return;
            }

            this._server.upload(this.newJournal)
                .then((journal) => {
                    const  found = this._list.some((j) => j.id === journal.id);
                    if (!found) {
                        this._list.unshift(journal);
                        this._total++;
                    }

                    this.newJournal = void 0;
                    $event.target.reset();
                });
        }

        isSortedBy(name) {
            return this._sortBy === name;
        }

        isSortOrderAscending() {
            return !this._descending;
        }

        isSortOrderDescending() {
            return this._descending;
        }

        sort(name) {
            if (this.isSortedBy(name)) {
                this._descending = !this._descending;
            } else {
                this._sortBy = name;
                this._descending = false;
            }
            this._page = 0;
            this._list = [];
            this.load();
        }

        getJournals() {
            return this._list;
        }

        getTotal() {
            return this._total;
        }

        formatTimestamp(journal) {
            if (!journal.timestamp) {
                return 'Queued for processing...';
            }

            return this._$filter('date')(journal.timestamp, 'mediumDate');
        }

        toggleInterest(journal) {
            if (!journal.timestamp) {
                return;
            }
            const action = journal.interested ? this._server.uninterest : this._server.interest;
            journal.interested = !journal.interested;
            action.apply(this._server, [journal.id]);
        }

        load() {
            if (this._list === void 0) {
                this._list = [];
            } else {
                if (this._list.length === this._total) {
                    return;
                }
            }

            this._page++;
            this._server.all(this._page, size, this._sortBy, this._descending ? 'desc' : 'asc')
                .then((pagedResult) => {
                    this._total = pagedResult.count;
                    pagedResult.list.forEach((item) => {
                        this._list.push(item);
                    });
                });
        }

        _onUpload(journal) {
            const  found = this._list.some((j) => j.id === journal.id);

            if (found) {
                return;
            }

            this._toastr.info(
                `${journal.publisher} has uploaded "${journal.name}".`,
                'New journal');

            this._list.unshift(journal);
            this._total++;
        }

        _onNotification(notification) {
           const getJournal = () => {
                const matched = this._list.find((journal) => {
                    return journal.id === notification.model.id;
                });
                return matched;
            };

            if ('PdfToImage' !== notification.jobName) {
                return;
            }

            this._$scope.$apply(() => {
                if ('started' === notification.eventName) {
                    const journal = getJournal();
                    if (!journal) {
                        return;
                    }
                    journal.job = notification.model;
                    journal.job.progress = 0;
                } else if ('processing' === notification.eventName) {
                    const journal = getJournal();
                    if (!journal) {
                        return;
                    }
                    journal.job = notification.model;
                    journal.job.progress = Math.ceil(
                            journal.job.processedPages / journal.job.totalPages
                        ) *
                        100;

                    if (journal.job.progress > 100) {
                        journal.job.progress = 100;
                    }
                } else if ('completed' === notification.eventName) {
                    const journal = getJournal();
                    if (!journal) {
                        return;
                    }
                    journal.timestamp = notification.model.timestamp;
                    delete journal['job'];
                }
            }); 
        }
    }

    controller.$inject = [
        '$scope',
        '$filter',
        '$window',
        'toastr',
        'server',
        'IsPublisher'
    ];

    app.controller('MainController', controller);
};