﻿export default (app, $) => {
    function run($rootScope) {
        const connection = $.hubConnection();

        const journalsHub = connection.createHubProxy('journalsHub');
        const backgroundJobNotificationsHub = connection.createHubProxy('backgroundJobNotificationsHub');

        journalsHub.on('uploaded',
        (model) => {
            $rootScope.$broadcast('mjFileUploaded', model);
        });

        backgroundJobNotificationsHub.on('notify',
        (jobName, eventName, model) => {
            $rootScope.$broadcast('mjBackgroundJobNotification',
            {
                jobName: jobName,
                eventName: eventName,
                model: model
            });
        });

        connection.start();
    }

    run.$inject = [
        '$rootScope'
    ];

    app.run(run);
};