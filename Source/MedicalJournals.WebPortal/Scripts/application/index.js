﻿const ng = require('angular');
const $ = require('jquery');
require('angular-animate');
require('angular-toastr');
require('ms-signalr-client');

const app = ng.module('medical-journals', [
    'ngAnimate',
    'toastr'
]);

require('./server.service')(app, ng);
require('./ajax-progress.component')(app);
require('./uploader.directive')(app);
require('./auto-scroll.directive')(app);
require('./main.controller')(app);
require('./hubs')(app, $);