﻿export default (app) => {

    function directive() {

        const template =
            '<div class="input-group">' +
                '<div class="input-group-btn">' +
                    '<label for="{{ name }}" class="btn btn-default">' +
                        '<i class="fa fa-folder-open"></i> {{ label }}' +
                    '</label>' +
                '</div>' +
                '<input class="form-control" readonly>' +
                '<input id="{{ name }}" name="{{ name }}" accept=".{{ extension }}" type="file" style="position:absolute;clip:rect(0px 0px 0px 0px)" {{ required ? "required" : "" }} tabindex="-1">' +
                '<div class="input-group-btn" ng-transclude><div/>' +
           '</div>';

        function link(scope, $element) {
            const $display = $element.find('input').eq(0);
            const $file = $element.find('input').eq(1);

            function fileChange() {
                const file = $file[0].files.length ? $file[0].files[0] : void 0;
                $display.val(file ? file.name : '');
                scope.$apply(() => {
                    scope.file = file;
                });
            }

            function displayClick() {
                $file[0].click();
            }

            $file.on('change', fileChange);
            $display.on('click', displayClick);

            scope.$on('$destroy',
            () => {
                $file.off('change', fileChange);
                $display.off('click', displayClick);
            });
        }

        return {
            scope: {
                name: '@',
                label: '@',
                extension: '@',
                required: '@',
                file: '=mjFileUploader'
            },
            restrict: 'EA',
            transclude: true,
            template: template,
            link: link
        };
    }

    app.directive('mjFileUploader', directive);
};