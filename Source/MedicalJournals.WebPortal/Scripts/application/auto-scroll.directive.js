﻿export default (app) => {
    function directive($window) {

        function debounce(action, delay) {
            let handle;

            return function() {
                const context = this;
                const args = arguments;

                if (handle) {
                    $window.clearTimeout(handle);
                }
                handle = $window.setTimeout(() => {
                        action.apply(context, args);
                    },
                    delay);
            };
        }

        function link(scope, $element) {

            const scroll = debounce(() => {
                    const e = $element[0];
                    let distance = parseInt(scope.distance, 10);

                    if (isNaN(distance)) {
                        distance = 0;
                    }

                    if (e.scrollTop + e.offsetHeight + distance >=
                        e.scrollHeight) {
                        scope.load();
                    }
                },
                250);

            $element.on('scroll', scroll);

            scope.$on('$destroy',
            () => {
                $element.off('scroll', scroll);
            });
        }

        return {
            scope: {
                distance: '@',
                load: '&mjAutoScroll'
            },
            link: link
        };
    }

    directive.$inject = [
        '$window'
    ];

    app.directive('mjAutoScroll', directive);
};