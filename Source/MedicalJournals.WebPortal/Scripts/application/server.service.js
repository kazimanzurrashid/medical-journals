﻿export default (app, ng) => {
    function interceptor($window) {
        return {
            request: (xhrConfig) => {
                const accessToken = $window.localStorage.getItem('accessToken');
                xhrConfig.headers.Authorization = `Bearer ${accessToken}`;
                return xhrConfig;
            }
        };
    }

    interceptor.$inject = [
        '$window'
    ];

    function config($httpProvider) {
        $httpProvider.interceptors.unshift(interceptor);
    }

    config.$inject = [
        '$httpProvider'
    ];

    const baseUrl = '/api';

    class server {
        constructor($http, $window) {
            this._$http = $http;
            this._$window = $window;
        }

        all(page, size, sortBy, sortOrder) {
            const offset = (page - 1) * size;
            const sort = sortBy + (sortOrder ? ` ${sortOrder}` : '');

            const params = {
                offset: offset,
                limit: size,
                sort
            };

            return this._$http.get(`${baseUrl}/journals`, { params })
                .then((response) => {
                    return response.data;
                });
        }

        upload(journal) {
            const form = new this._$window.FormData();
            form.append('journal', journal);
            return this._$http.post(
                    '/files',
                    form,
                    {
                        transformRequest: ng.identity,
                        headers: {
                            'Content-Type': void 0
                        }
                    })
                .then((response) => {
                    return response.data;
                });
        }

        interest(id) {
            return this._$http.post(`${baseUrl}/journals/${id}/interest`);
        }

        uninterest(id) {
            return this._$http.delete(`${baseUrl}/journals/${id}/interest`);
        }

        signout() {
            return this._$http.post(`${baseUrl}/sign-out`)
                .then(() => {
                    this._$window.localStorage.removeItem('accessToken');
                });
        }
    }

    server.$inject = [
        '$http',
        '$window'
    ];

    app.config(config).service('server', server);
};