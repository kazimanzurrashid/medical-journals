﻿export default ($) => {
    $(() => {
        $('.flash-bar')
            .on('click',
                'button.close',
                (e) => {
                    $(e.currentTarget)
                        .parent()
                        .slideUp(function() {
                            $(this).remove();
                        });
                })
            .each(function(index, el) {
                const $flashbar = $(el);
                const duration = parseInt(($flashbar.attr('data-duration') || '7000'), 10);

                setTimeout(() => {
                        $flashbar.slideUp('slow',
                        () => {
                            $flashbar.remove();
                        });
                    },
                    duration);
            });
    });
};