﻿const $ = require('jquery');
require('jquery-validation');
require('jquery-validation-unobtrusive');

require('./jquery.validate.unobtrusive.overrides')($);
require('./flash-bar')($);
require('./nav-bar')($);
require('./sign-in')($);