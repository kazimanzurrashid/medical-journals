﻿export default ($) => {
    $(() => {
        const currentUrl = window.location.href.toLowerCase();

        $('#navigation li > a')
            .each((index, el) => {
                const $anchor = $(el);
                const anchorUrl = $anchor.prop('href').toLowerCase();
                if (currentUrl.indexOf(anchorUrl) > -1) {
                    $anchor.parent().addClass('active');
                }
            });
    });
};