﻿export default ($) => {
    $(() => {
        function submit(e) {
            const message = $.parseHTML('<p class="text-danger">' +
                '<i class="fa fa-exclamation-circle"></i> ' +
                '<strong>Error!</strong> Invalid credentials.' +
                '</p>');

            const $form = $(e.currentTarget);
            $form.find('p.text-danger').remove();

            e.preventDefault();

            $.ajax({
                    method: $form.prop('method'),
                    url: $form.prop('action'),
                    data: $form.serialize(),
                    contentType: 'application/x-www-form-urlencoded'
                })
                .success((response) => {
                    window.localStorage.setItem('accessToken', response.access_token);
                    window.location = '/dashboard';
                })
                .error(() => {
                    $(message).insertBefore($form.find('.form-group').last());
                });
        }

        $('#sign-in-form').on('submit', submit);
    });
};