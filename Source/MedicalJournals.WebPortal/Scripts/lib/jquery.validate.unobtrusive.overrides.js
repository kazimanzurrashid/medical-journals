﻿export default ($) => {
    $(() => {
        $('form')
            .each((index, item) => {
                const validator = $(item).data('validator');

                if (!validator) {
                    return;
                }

                validator.settings.highlight = (el) => {
                    $(el).closest('.form-group').addClass('has-error');
                };

                validator.settings.unhighlight = (el) => {
                    $(el).closest('.form-group').removeClass('has-error');
                };
            });
    });
};