﻿namespace MedicalJournals.BackgroundJobProcessor
{
    using System.Linq;

    using Autofac;

    using Serilog;

    using Topshelf;
    using Topshelf.Autofac;

    using Infrastructure;

    public static class Program
    {
        public static void Main()
        {
            var container = CreateContainer();
            var logger = container.Resolve<ILogger>();

            HostFactory.Run(config =>
            {
                config.SetDescription("Background job processing, runs all long running operations.");
                config.StartAutomaticallyDelayed();

                config.UseAutofacContainer(container);
                config.UseSerilog();

                config.Service<Runners>(
                service =>
                    {
                        service.ConstructUsingAutofacContainer();

                        service.WhenStarted((m, c) => m.Start());
                        service.WhenStopped((m, c) => m.Stop());

                        service.AfterStartingService(ctx => logger.Information("Background job processor started."));
                        service.AfterStoppingService(ctx => logger.Information("Background job processor stopped."));
                    });
            });
        }

        private static IContainer CreateContainer()
        {
            var assemblies = AssembliesProvider
                .Instance
                .Assemblies
                .ToArray();

            var builder = new ContainerBuilder();

            builder.RegisterAssemblyModules(assemblies);

            var container = builder.Build();

            return container;
        }
    }
}