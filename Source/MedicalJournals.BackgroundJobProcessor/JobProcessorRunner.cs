namespace MedicalJournals.BackgroundJobProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using Serilog;

    using Infrastructure;

    public class JobProcessorRunner : Disposable, IRunner
    {
        private readonly ILogger logger;
        private readonly IEnumerable<IJobProcessor> jobProcessors;

        private CancellationTokenSource cancellation;

        public JobProcessorRunner(
            ILogger logger,
            IEnumerable<IJobProcessor> jobProcessors)
        {
            this.logger = logger;
            this.jobProcessors = jobProcessors;
        }

        public bool Start()
        {
            if (this.cancellation != null)
            {
                return true;
            }

            this.cancellation = new CancellationTokenSource();
            var cancellationToken = this.cancellation.Token;

            try
            {
                var tasks = this.jobProcessors
                    .Select(jp => jp.ProcessAsync(cancellationToken));

                Task.Run(() => Task.WhenAll(tasks), cancellationToken);
            }
            catch (OperationCanceledException)
            {
                // Eat it
            }
            catch (AggregateException ae)
            {
                this.logger.Error(
                    ae.GetBaseException(),
                    "The following exception has occurred while processing jobs.");
            }

            return true;
        }

        public bool Stop()
        {
            this.Cleanup();
            return true;
        }

        protected override void DisposeCore()
        {
            this.Cleanup();
            base.DisposeCore();
        }

        private void Cleanup()
        {
            if (this.cancellation == null)
            {
                return;
            }

            this.cancellation.Cancel();
            this.cancellation.Dispose();
            this.cancellation = null;
        }
    }
}