﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("MedicalJournals.BackgroundJobProcessor")]
[assembly: AssemblyProduct("MedicalJournals.BackgroundJobProcessor")]
[assembly: CLSCompliant(true)]
[assembly: Guid("8f51dbe1-03f2-47a9-8711-9b7b3e95c2fb")]
[assembly: NeutralResourcesLanguage("en-US")]