﻿namespace MedicalJournals.BackgroundJobProcessor
{
    using System;

    public interface IRunner : IDisposable
    {
        bool Start();

        bool Stop();
    }
}