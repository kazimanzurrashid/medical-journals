﻿namespace MedicalJournals.BackgroundJobProcessor
{
    using System.Collections.Generic;
    using System.Linq;

    public class Runners
    {
        private readonly IEnumerable<IRunner> runners;

        public Runners(IEnumerable<IRunner> runners)
        {
            this.runners = runners;
        }

        public bool Start()
        {
            return this.runners.All(r => r.Start());
        }

        public bool Stop()
        {
            return this.runners.All(r => r.Stop());
        }
    }
}