namespace MedicalJournals.Infrastructure
{
    using System;
    using System.Configuration;
    using System.IO;

    using Autofac;
    using Serilog;

    using BackgroundJobProcessor;
    using ExtensionMethods;

    public class ServiceRegistrations : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            RegisterDataContext(builder);
            RegisterRunners(builder);
            RegisterLogger(builder);
        }

        private static void RegisterDataContext(ContainerBuilder builder)
        {
            var newLine = Environment.NewLine;

            builder.Register<DataContext>()
                .WithParameter("name", ConfigurationManager.AppSettings["app.database"])
                .OnActivated(
                    e =>
                    {
                        var context = e.Context.Resolve<IComponentContext>();

                        e.Instance.Database.Log = s =>
                        {
                            if (string.IsNullOrWhiteSpace(s))
                            {
                                return;
                            }

                            var arg = s;

                            while (arg.EndsWith(newLine, StringComparison.Ordinal))
                            {
                                arg = arg.Substring(0, arg.Length - newLine.Length);
                            }

                            context.Resolve<ILogger>().Debug("{EfLog}", arg);
                        };
                    });
        }

        private static void RegisterRunners(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(IRunner).Assembly)
                .AssignableTo<IRunner>()
                .AsSelf()
                .AsImplementedInterfaces();

            builder.Register<Runners>().SingleInstance();
        }

        private static void RegisterLogger(ContainerBuilder builder)
        {
            const string template = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} " +
                "{MachineName}::{ProcessId}::{ThreadId} " +
                "[{Level}] " +
                "{Message}{NewLine}{Exception}";

            var logFormat = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs") +
                Path.DirectorySeparatorChar + "{Date}.txt";

            var seriLogger = new LoggerConfiguration()
                .ReadFrom.AppSettings()
                .WriteTo.RollingFile(logFormat, outputTemplate: template)
                .WriteTo.LiterateConsole(outputTemplate: template)
                .Enrich.FromLogContext()
                .Enrich.WithMachineName()
                .Enrich.WithProcessId()
                .Enrich.WithThreadId()
                .CreateLogger();

            builder.RegisterInstance(seriLogger)
                .AsSelf()
                .AsImplementedInterfaces()
                .SingleInstance();
        }
    }
}