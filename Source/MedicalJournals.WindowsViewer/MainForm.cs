﻿namespace MedicalJournals.WindowsViewer
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public interface IMainView
    {
        Journal SelectedJournal { get; }

        Task View();

        bool ShowSignIn();

        void ShowPreview();
    }

    public partial class MainForm : Form, IMainView
    {
        private readonly IResolver resolver;
        private readonly IServerProxy server;

        private readonly SynchronizationContext synchronizationContext;

        public MainForm()
        {
            this.InitializeComponent();
            this.synchronizationContext = SynchronizationContext.Current;
        }

        public MainForm(IResolver resolver, IServerProxy server) : this()
        {
            this.resolver = resolver;
            this.server = server;
        }

        public virtual Journal SelectedJournal
        {
            get
            {
                if (this.JournalListView.SelectedItems.Count == 0)
                {
                    return null;
                }

                return (Journal)this.JournalListView.SelectedItems[0].Tag;
            }
        }

        public virtual bool ShowSignIn()
        {
            using (var signIn = this.resolver
                .Resolve<ISignInView>())
            {
                signIn.View();

                return signIn.Valid;
            }
        }

        public virtual void ShowPreview()
        {
            var journal = this.SelectedJournal;

            if (journal == null)
            {
                return;
            }

            using (var viewer = this.resolver.Resolve<IViewerView>())
            {
                viewer.View(journal);
            }
        }

        public virtual async Task View()
        {
            if (!this.ShowSignIn())
            {
                this.Close();
                this.Dispose();
                Application.Exit();
                return;
            }

            this.Cursor = Cursors.WaitCursor;

            var list = await this.server.GetJournals();

            this.synchronizationContext.Post(
                state =>
                    {
                        var journals = (IEnumerable<Journal>)state;

                        this.JournalListView.BeginUpdate();

                        foreach (var journal in journals)
                        {
                            var item = new ListViewItem
                            {
                                Text = journal.Name,
                                Tag = journal
                            };

                            item.SubItems.Add(new ListViewItem.ListViewSubItem
                            {
                                Text = journal.Publisher
                            });

                            item.SubItems.Add(new ListViewItem.ListViewSubItem
                            {
                                Text = journal.TotalPages.ToString()
                            });

                            this.JournalListView.Items.Add(item);
                        }

                        if (this.JournalListView.Items.Count > 0)
                        {
                            this.JournalListView.Items[0].Selected = true;
                        }

                        this.JournalListView.EndUpdate();
                    },
               list);

            this.Cursor = Cursors.Default;
        }

        private async void OnFormLoad(
            object sender,
            EventArgs e)
        {
            await this.View();
        }

        private void OnListViewDoubleClick(
            object sender, 
            EventArgs e)
        {
            this.ShowPreview();
        }
    }
}