namespace MedicalJournals.WindowsViewer
{
    using System;

    public interface IResolver
    {
        T Resolve<T>();
    }

    public class Resolver : IResolver
    {
        private readonly Func<Type, object> factory;

        public Resolver(Func<Type, object> factory)
        {
            this.factory = factory;
        }

        public T Resolve<T>()
        {
            return (T)this.factory(typeof(T));
        }
    }
}