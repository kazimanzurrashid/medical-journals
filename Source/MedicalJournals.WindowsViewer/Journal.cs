namespace MedicalJournals.WindowsViewer
{
    public class Journal
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Publisher { get; set; }

        public string Url { get; set; }

        public int TotalPages { get; set; }
    }
}