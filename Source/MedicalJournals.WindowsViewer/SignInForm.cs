﻿namespace MedicalJournals.WindowsViewer
{
    using System;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public interface ISignInView : IDisposable
    {
        bool Valid { get; }

        void View();

        Task Signin();

        void ShowErrorMessage(string message);
    }

    public partial class SignInForm : Form, ISignInView
    {
        private readonly IServerProxy server;

        public SignInForm()
        {
            this.InitializeComponent();
        }

        public SignInForm(IServerProxy server) : this()
        {
            this.server = server;
        }

        public bool Valid => this.DialogResult == DialogResult.OK;

        public void View()
        {
            this.ShowDialog();
        }

        public virtual async Task Signin()
        {
            if (string.IsNullOrWhiteSpace(this.EmailTextBox.Text))
            {
                this.EmailTextBox.Focus();
                this.ShowErrorMessage("Email is required.");
                return;
            }

            if (string.IsNullOrWhiteSpace(this.PasswordTextBox.Text))
            {
                this.PasswordTextBox.Focus();
                this.ShowErrorMessage("Password is required.");
                return;
            }

            this.Cursor = Cursors.WaitCursor;

            var valid = await this.server.SignIn(
                this.EmailTextBox.Text,
                this.PasswordTextBox.Text);

            this.Cursor = Cursors.Default;

            if (!valid)
            {
                this.EmailTextBox.SelectAll();
                this.PasswordTextBox.SelectAll();
                this.EmailTextBox.Focus();
                this.ShowErrorMessage("Invalid credential.");
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        public virtual void ShowErrorMessage(string message)
        {
                MessageBox.Show(
                    this, 
                    message, 
                    @"Error", 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Exclamation);
        }

        private async void OnSignInButonClick(
            object sender,
            EventArgs e)
        {
            await this.Signin();
        }
    }
}