﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("MedicalJournals.WindowsViewer")]
[assembly: AssemblyProduct("MedicalJournals.WindowsViewer")]
[assembly: CLSCompliant(true)]
[assembly: Guid("eb7ce0e2-ce88-4284-930d-8391525f5d09")]
[assembly: NeutralResourcesLanguage("en-US")]