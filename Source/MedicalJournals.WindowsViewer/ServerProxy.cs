namespace MedicalJournals.WindowsViewer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;

    using Newtonsoft.Json.Linq;

    public interface IServerProxy : IDisposable
    {
        Task<bool> SignIn(
            string email,
            string password);

        Task<IEnumerable<Journal>> GetJournals();
    }

    public class ServerProxy : IServerProxy
    {
        private readonly HttpClient client;

        private bool disposed;

        public ServerProxy(string endpoint)
        {
            this.client = new HttpClient
            {
                BaseAddress = new Uri(endpoint)
            };

            this.client.DefaultRequestHeaders.Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        ~ServerProxy()
        {
            this.Dispose(false);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task<bool> SignIn(
            string email,
            string password)
        {
            var payload = new FormUrlEncodedContent(
                new Dictionary<string, string>
            {
                { "UserName", email },
                { "Password", password },
                { "grant_type", "password" }
            });

            var result = await this.client
                .PostAsync("/token", payload);

            if (!result.IsSuccessStatusCode)
            {
                return false;
            }

            var content = await result.Content.ReadAsAsync<JObject>();
            var accessToken = (string)content["access_token"];
            this.client.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);

            return true;
        }

        public async Task<IEnumerable<Journal>> GetJournals()
        {
            var response = await this.client
                .GetAsync("/api/journals/interested");

            if (!response.IsSuccessStatusCode)
            {
                return Enumerable.Empty<Journal>();
            }

            return await response
                .Content
                .ReadAsAsync<IEnumerable<Journal>>();
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed && disposing)
            {
                this.client.Dispose();
            }

            this.disposed = true;
        }
    }
}