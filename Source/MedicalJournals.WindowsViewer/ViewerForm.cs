﻿namespace MedicalJournals.WindowsViewer
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    public interface IViewerView : IDisposable
    {
        Journal CurrentJournal { get; }

        int CurrentPage { get; }

        bool CanMovePrevious { get; }

        bool CanMoveNext { get; }

        void View(Journal journal);

        void MovePrevious();

        void MoveNext();
    }

    public partial class ViewerForm : Form, IViewerView
    {
        public ViewerForm()
        {
            this.InitializeComponent();
        }

        public Journal CurrentJournal { get; private set; }

        public int CurrentPage { get; private set; }

        public bool CanMovePrevious => 
            this.CurrentPage > 1;

        public bool CanMoveNext => 
            this.CurrentPage < this.CurrentJournal.TotalPages;

        public virtual void View(Journal journal)
        {
            this.CurrentJournal = journal;
            this.CurrentPage = 1;
            this.UpdateNavigationButtons();
            this.PreviewPictureBox.ImageLocation = this.GetPageUrl();
            this.ShowDialog();
        }

        public virtual void MoveNext()
        {
            this.CurrentPage++;
            this.PreviewPictureBox.ImageLocation = this.GetPageUrl();
            this.UpdateNavigationButtons();
        }

        public virtual void MovePrevious()
        {
            this.CurrentPage--;
            this.PreviewPictureBox.ImageLocation = this.GetPageUrl();
            this.UpdateNavigationButtons();
        }

        private void UpdateNavigationButtons()
        {
            this.PreviousButton.Enabled = this.CanMovePrevious;
            this.NextButton.Enabled = this.CanMoveNext;
        }

        private string GetPageUrl()
        {
            return this.CurrentJournal.Url + "/" + this.CurrentPage + ".jpg";
        }

        private void OnPreviousButtonClick(object sender, EventArgs e)
        {
            this.MovePrevious();
        }

        private void OnNextButtonClick(object sender, EventArgs e)
        {
            this.MoveNext();
        }

        private void OnPreviewLoadCompleted(
            object sender,
            AsyncCompletedEventArgs e)
        {
            this.Height = this.ToolBar.Height + 
                this.PreviewPictureBox.Image.Height;
            this.Width = this.PreviewPictureBox.Image.Width;
            this.CenterToScreen();
        }
    }
}