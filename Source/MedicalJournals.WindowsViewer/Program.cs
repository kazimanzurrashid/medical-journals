﻿namespace MedicalJournals.WindowsViewer
{
    using System;
    using System.Configuration;
    using System.Windows.Forms;

    using Autofac;

    public static class Program
    {
        [STAThread]
        public static void Main()
        {
            var container = CreateContainer();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(container.Resolve<MainForm>());

            container.Dispose();
        }

        private static IContainer CreateContainer()
        {
            var builder = new ContainerBuilder();

            builder.Register<Func<Type, object>>(
                c =>
                {
                    var ctx = c.Resolve<IComponentContext>();
                    return ctx.Resolve;
                });

            builder.RegisterType<Resolver>()
                .AsImplementedInterfaces()
                .AsSelf()
                .SingleInstance();

            builder.RegisterType<ServerProxy>()
                .AsImplementedInterfaces()
                .AsSelf()
                .WithParameter("endpoint", ConfigurationManager.AppSettings["server.url"])
                .SingleInstance();

            builder.RegisterAssemblyTypes(typeof(Program).Assembly)
                .Where(t => !t.IsAbstract && typeof(Form).IsAssignableFrom(t))
                .AsImplementedInterfaces()
                .AsSelf();

            var container = builder.Build();

            return container;
        }
    }
}