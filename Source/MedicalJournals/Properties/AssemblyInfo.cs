﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("MedicalJournals")]
[assembly: AssemblyProduct("MedicalJournals")]
[assembly: CLSCompliant(true)]
[assembly: Guid("cd4f1b2b-efbf-4341-92a1-0731cfccc8b4")]
[assembly: NeutralResourcesLanguage("en-US")]