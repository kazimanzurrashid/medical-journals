namespace MedicalJournals.Models
{
    public class ActionCommandResult :
        ActionCommandResultBase<ActionCommandResult>
    {
        public ActionCommandResult()
        {
        }

        public ActionCommandResult(params string[] errors)
            : base(errors)
        {
        }

        public static ActionCommandResult Success { get; } = 
            new ActionCommandResult();
    }
}