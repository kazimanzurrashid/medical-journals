namespace MedicalJournals.Models
{
    public sealed class EmptyCommand
    {
        private EmptyCommand()
        {
        }

        public static EmptyCommand Instance { get; } = new EmptyCommand();
    }
}