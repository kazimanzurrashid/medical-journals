﻿namespace MedicalJournals.Models
{
    using System.IO;

    public class JournalCreateCommand
    {
        public string Name { get; set; }

        public Stream Content { get; set; }
    }
}