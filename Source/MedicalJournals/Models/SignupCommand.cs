﻿namespace MedicalJournals.Models
{
    using System.ComponentModel.DataAnnotations;

    public class SignupCommand
    {
        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress]
        [UIHint("UserName")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Required(ErrorMessage = "Confirm password is required.")]
        [Compare("Password", ErrorMessage = "Confirm password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "I am also a publisher")]
        public bool IsPublisher { get; set; }
    }
}