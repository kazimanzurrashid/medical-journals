namespace MedicalJournals.Models
{
    using System.Collections.Generic;

    public class PagedListResult<TModel>
        where TModel : class
    {
        public IEnumerable<TModel> List { get; set; }

        public long Count { get; set; }
    }
}