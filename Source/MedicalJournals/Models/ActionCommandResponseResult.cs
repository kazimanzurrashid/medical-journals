namespace MedicalJournals.Models
{
    public class ActionCommandResponseResult<TResponse> :
        ActionCommandResultBase<ActionCommandResponseResult<TResponse>>
    {
        public ActionCommandResponseResult()
        {
        }

        public ActionCommandResponseResult(params string[] errors)
            : base(errors)
        {
        }

        public TResponse Response { get; set; }
    }
}