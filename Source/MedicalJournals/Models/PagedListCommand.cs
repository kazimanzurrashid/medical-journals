namespace MedicalJournals.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.Linq;

    public class PagedListCommand : IValidatableObject
    {
        private static readonly IEnumerable<string> SortOrders = new[] { "asc", "desc" };

        public string Condition { get; set; }

        public int? Offset { get; set; }

        public int? Limit { get; set; }

        public string Sort { get; set; }

        public virtual string GetOrderByClause()
        {
            if (string.IsNullOrWhiteSpace(this.Sort))
            {
                return null;
            }

            var sort = this.Sort.Split(
                new[] { ' ' },
                StringSplitOptions.RemoveEmptyEntries);

            if (sort.Length == 0)
            {
                return null;
            }

            if (sort.Length == 1)
            {
                return sort.First();
            }

            return sort[0] + " " + sort[1];
        }

        public virtual IEnumerable<ValidationResult> Validate(
            ValidationContext validationContext)
        {
            if (!string.IsNullOrWhiteSpace(this.Sort))
            {
                var sort = this.Sort.Split(
                    new[] { ' ' },
                    StringSplitOptions.RemoveEmptyEntries);

                if (sort.Length <= 1 ||
                    SortOrders.Contains(sort[1], StringComparer.OrdinalIgnoreCase))
                {
                    yield break;
                }

                var sortOrderErrorMessage = string.Format(
                    CultureInfo.CurrentCulture,
                    "Invalid sort order, only supports {0}.",
                    string.Join(", ", SortOrders));

                yield return new ValidationResult(sortOrderErrorMessage);
            }

            if (this.Offset.GetValueOrDefault() < 0)
            {
                yield return new ValidationResult(
                    "Offset cannot be a negative integer.",
                    new[] { "Offset" });
            }

            if (this.Limit == null)
            {
                yield break;
            }

            if (this.Limit.GetValueOrDefault() < 1)
            {
                yield return new ValidationResult(
                    "Limit must be a positive integer.",
                    new[] { "Limit" });
            }

            if (this.Limit.GetValueOrDefault() > 100)
            {
                yield return new ValidationResult(
                    "Limit cannot exceed 100.",
                    new[] { "Limit" });
            }
        }
    }
}