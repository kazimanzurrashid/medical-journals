﻿namespace MedicalJournals.Models
{
    public class InterestDeleteCommand
    {
        public string JournalId { get; set; }
    }
}