﻿namespace MedicalJournals.Models
{
    using System;

    public class JournalListItem
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Publisher { get; set; }

        public DateTime? Timestamp { get; set; }

        public bool Interested { get; set; }
    }
}