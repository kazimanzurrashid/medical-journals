namespace MedicalJournals.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public abstract class ActionCommandResultBase<TCommandResult>
        where TCommandResult : ActionCommandResultBase<TCommandResult>
    {
        private readonly IDictionary<string, IList<string>> errors =
            new Dictionary<string, IList<string>>(
                StringComparer.OrdinalIgnoreCase);

        protected ActionCommandResultBase()
        {
        }

        protected ActionCommandResultBase(params string[] errors)
        {
            this.AppendErrors(string.Empty, errors);
        }

        public static implicit operator bool(
            ActionCommandResultBase<TCommandResult> result)
        {
            return (result != null) && result.ToBoolean();
        }

        public virtual IEnumerable<Pair<string, IEnumerable<string>>> AllErrors()
        {
            return this.errors.Select(pair =>
                new Pair<string, IEnumerable<string>>
                {
                    Key = pair.Key,
                    Value = pair.Value
                });
        }

        public virtual TCommandResult ClearAllErrors()
        {
            this.errors.Clear();

            return (TCommandResult)this;
        }

        public virtual TCommandResult AddErrors(
            string propertyName,
            params string[] errorMessage)
        {
            this.AppendErrors(propertyName, errorMessage);

            return (TCommandResult)this;
        }

        public virtual IEnumerable<string> Errors(string propertyName)
        {
            var key = propertyName ?? string.Empty;

            if (!this.errors.ContainsKey(key))
            {
                yield break;
            }

            foreach (var error in this.errors[key])
            {
                yield return error;
            }
        }

        public virtual TCommandResult ClearErrors(string propertyName)
        {
            var key = propertyName ?? string.Empty;

            if (this.errors.ContainsKey(key))
            {
                this.errors.Remove(key);
            }

            return (TCommandResult)this;
        }

        public bool ToBoolean()
        {
            return !this.errors.Any();
        }

        private void AppendErrors(
            string propertyName,
            IEnumerable<string> errorMessages)
        {
            var key = propertyName ?? string.Empty;
            IList<string> messages;

            if (!this.errors.TryGetValue(key, out messages))
            {
                messages = new List<string>();
                this.errors.Add(key, messages);
            }

            foreach (var mesage in errorMessages)
            {
                messages.Add(mesage);
            }
        }
    }
}