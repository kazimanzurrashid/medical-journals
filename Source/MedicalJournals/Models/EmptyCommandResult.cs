namespace MedicalJournals.Models
{
    public sealed class EmptyCommandResult
    {
        private EmptyCommandResult()
        {
        }

        public static EmptyCommandResult Instance { get; } = new EmptyCommandResult();
    }
}