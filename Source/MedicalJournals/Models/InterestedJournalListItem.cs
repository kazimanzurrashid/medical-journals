﻿namespace MedicalJournals.Models
{
    public class InterestedJournalListItem
    {
        public string Id { get; set; }

        public string Name { get; set; }
        
        public string Publisher { get; set; }

        public string Url { get; set; }

        public int TotalPages { get; set; }
    }
}