namespace MedicalJournals
{
    using System;

    public abstract class Disposable : IDisposable
    {
        private bool disposed;

        ~Disposable()
        {
            this.Dispose(false);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void DisposeCore()
        {
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed && disposing)
            {
                this.DisposeCore();
            }

            this.disposed = true;
        }
    }
}