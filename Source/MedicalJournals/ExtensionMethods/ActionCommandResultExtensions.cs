namespace MedicalJournals.ExtensionMethods
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Validation;
    using System.Linq;

    using Models;

    public static class ActionCommandResultExtensions
    {
        public static TCommandResult CopyErrorsFrom<TCommandResult>(
            this TCommandResult instance,
            IEnumerable<DbEntityValidationResult> source)
            where TCommandResult : ActionCommandResultBase<TCommandResult>
        {
            if (instance == null)
            {
                throw new ArgumentNullException(nameof(instance));
            }

            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            foreach (var error in source.SelectMany(e => e.ValidationErrors))
            {
                instance.AddErrors(error.PropertyName, error.ErrorMessage);
            }

            return instance;
        }
    }
}