namespace MedicalJournals.ExtensionMethods
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public static class ObjectExtensions
    {
        private static readonly JsonSerializerSettings ServerJsonSettings = 
            CreateServerJsonSettings();

        private static readonly JsonSerializerSettings ClientJsonSettings = 
            CreateClientJsonSettings();

        public static IDictionary<string, object> ToDictionary(
            this object instance)
        {
            if (instance == null)
            {
                return new Dictionary<string, object>();
            }

            var result = TypeDescriptor.GetProperties(instance.GetType())
                .Cast<PropertyDescriptor>()
                .ToDictionary(p => p.Name, p => p.GetValue(instance));

            return result;
        }

        public static string ToServerJson(this object instance)
        {
            return instance == null ?
                null :
                JsonConvert.SerializeObject(instance, ServerJsonSettings);
        }

        public static T FromServerJson<T>(this string instance)
        {
            return (T)FromServerJson(instance, typeof(T));
        }

        public static object FromServerJson(
            this string instance,
            Type type)
        {
            return string.IsNullOrWhiteSpace(instance) ?
                null :
                JsonConvert.DeserializeObject(instance, type, ServerJsonSettings);
        }

        public static string ToClientJson(this object instance)
        {
            return instance == null ?
                null :
                JsonConvert.SerializeObject(instance, ClientJsonSettings);
        }

        public static T FromClientJson<T>(this string instance)
        {
            return (T)FromClientJson(instance, typeof(T));
        }

        public static object FromClientJson(
            this string instance,
            Type type)
        {
            return string.IsNullOrWhiteSpace(instance) ?
                null :
                JsonConvert.DeserializeObject(instance, type, ClientJsonSettings);
        }

        private static JsonSerializerSettings CreateClientJsonSettings()
        {
            var settings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            return settings;
        }

        private static JsonSerializerSettings CreateServerJsonSettings()
        {
            var settings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateTimeZoneHandling = DateTimeZoneHandling.Utc
            };

            return settings;
        }
    }
}