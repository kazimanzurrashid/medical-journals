﻿namespace MedicalJournals.ExtensionMethods
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    using Infrastructure;
    using Models;

    public static class CommandBusExtensions
    {
        public static Task SendAsync(
            this ICommandBus instance,
            object command,
            CancellationToken cancellationToken)
        {
            if (instance == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            return instance.SendAsync<EmptyCommandResult>(
                command,
                cancellationToken);
        }
    }
}