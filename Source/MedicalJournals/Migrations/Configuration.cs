namespace MedicalJournals.Migrations
{
    using System.Data.Entity.Migrations;
    using Infrastructure;

    internal sealed class Configuration : DbMigrationsConfiguration<DataContext>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataContext context)
        {
        }
    }
}