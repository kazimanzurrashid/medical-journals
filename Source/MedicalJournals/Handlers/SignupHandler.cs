namespace MedicalJournals.Handlers
{
    using System;
    using System.Data.Entity.Validation;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity;

    using DomainObjects;
    using ExtensionMethods;
    using Infrastructure;
    using Models;

    public class SignupHandler : 
        IAsyncCommandHandler<SignupCommand, ActionCommandResult>
    {
        private readonly UserManager<User> userManager;
        private readonly IDataContext dataContext;

        public SignupHandler(
            UserManager<User> userManager, 
            IDataContext dataContext)
        {
            this.userManager = userManager;
            this.dataContext = dataContext;
        }

        public async Task<ActionCommandResult> HandleAsync(
            SignupCommand command,
            CancellationToken cancellationToken)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            cancellationToken.ThrowIfCancellationRequested();

            var user = new User
            {
                UserName = command.Email,
                Email = command.Email,
                IsPublisher = command.IsPublisher,
                LockoutEnabled = true,
                EmailConfirmed = true
            };

            var createResult = await this.userManager
                .CreateAsync(user, command.Password);

            if (!createResult.Succeeded)
            {
                return new ActionCommandResult(
                    createResult.Errors.ToArray());
            }

            try
            {
                await this.dataContext.SaveChangesAsync(cancellationToken);
            }
            catch (DbEntityValidationException e)
            {
                return new ActionCommandResult()
                    .CopyErrorsFrom(e.EntityValidationErrors);
            }

            return ActionCommandResult.Success;
        }
    }
}