﻿namespace MedicalJournals.Handlers
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Dynamic;
    using System.Threading;
    using System.Threading.Tasks;

    using DomainObjects;
    using Infrastructure;
    using Models;

    public class JournalPagedListHandler :
        IAsyncCommandHandler<PagedListCommand, PagedListResult<JournalListItem>>
    {
        private readonly ICurrentUserProvider currentUserProvider;
        private readonly IDataContext dataContext;

        public JournalPagedListHandler(
            ICurrentUserProvider currentUserProvider,
            IDataContext dataContext)
        {
            this.currentUserProvider = currentUserProvider;
            this.dataContext = dataContext;
        }

        public async Task<PagedListResult<JournalListItem>> HandleAsync(
            PagedListCommand command,
            CancellationToken cancellationToken)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            cancellationToken.ThrowIfCancellationRequested();

            var currentUser = await this.currentUserProvider
                .GetAsync(cancellationToken);

            var query = this.dataContext
                .Get<Journal>()
                .Select(j => new JournalListItem
                {
                    Id = j.Id,
                    Name = j.Name,
                    Publisher = j.UploadedBy.Email,
                    Timestamp = j.AvailableAt,
                    Interested = j.Interests.Any(i => 
                        i.UserId == currentUser.Id)
                });

            var count = await query.LongCountAsync(cancellationToken);

            var orderBy = command.GetOrderByClause();

            if (string.IsNullOrWhiteSpace(orderBy))
            {
                orderBy = "Timestamp desc";
            }

            query = query.OrderBy(orderBy)
                .Skip(command.Offset.GetValueOrDefault());

            if (command.Limit.GetValueOrDefault() > 0)
            {
                query = query.Take(command.Limit.GetValueOrDefault());
            }

            var list = await query.ToListAsync(cancellationToken);

            return new PagedListResult<JournalListItem>
            {
                Count = count,
                List = list
            };
        }
    }
}