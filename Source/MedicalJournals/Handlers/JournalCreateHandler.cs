﻿namespace MedicalJournals.Handlers
{
    using System;
    using System.Data.Entity.Validation;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;

    using Serilog;

    using DomainObjects;
    using ExtensionMethods;
    using Infrastructure;
    using Models;

    public class JournalCreateHandler : 
        IAsyncCommandHandler<JournalCreateCommand, ActionCommandResponseResult<JournalListItem>>
    {
        private readonly ICurrentUserProvider currentUserProvider;
        private readonly IDataContext dataContext;
        private readonly IBlobStorage blobStorage;
        private readonly IJobQueue jobQueue;
        private readonly INewJournalUploadNotifier newJournalUploadNotifier;

        private readonly ILogger logger;

        public JournalCreateHandler(
            ICurrentUserProvider currentUserProvider,
            IDataContext dataContext,
            IBlobStorage blobStorage,
            IJobQueue jobQueue,
            INewJournalUploadNotifier newJournalUploadNotifier,
            ILogger logger)
        {
            this.currentUserProvider = currentUserProvider;
            this.dataContext = dataContext;
            this.blobStorage = blobStorage;
            this.jobQueue = jobQueue;
            this.newJournalUploadNotifier = newJournalUploadNotifier;
            this.logger = logger;
        }

        public async Task<ActionCommandResponseResult<JournalListItem>> HandleAsync(
            JournalCreateCommand command,
            CancellationToken cancellationToken)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            cancellationToken.ThrowIfCancellationRequested();

            var id = Guid.NewGuid().ToString("N").ToLowerInvariant();
            var name = Path.GetFileName(command.Name);
            var fullName = id + Path.AltDirectorySeparatorChar + name;

            var location = await this.blobStorage
                .UploadAsync(fullName, command.Content, cancellationToken);

            var currentUser = await this.currentUserProvider
                .GetAsync(cancellationToken);

            var journal = new Journal
            {
                Id = id,
                Name = Path.GetFileNameWithoutExtension(command.Name),
                Location = location,
                UploadById = currentUser.Id,
                UploadedAt = Clock.Now()
            };

            this.dataContext.Get<Journal>().Add(journal);

            try
            {
                await this.dataContext.SaveChangesAsync(cancellationToken);
            }
            catch (DbEntityValidationException e)
            {
                return new ActionCommandResponseResult<JournalListItem>()
                    .CopyErrorsFrom(e.EntityValidationErrors);
            }
            catch (Exception e)
            {
                const string Message = "The following exception has occurred" +
                    " while saving journal in the database.";

                this.logger.Error(e, Message);
                throw;
            }

            await this.jobQueue.EnqueueAsync(
                new JournalPdfToImageConvertCommand { JournalId = id },
                cancellationToken);

            var model = new JournalListItem
                            {
                                Id = id,
                                Name = journal.Name,
                                Publisher = currentUser.Name
                            };

            await this.newJournalUploadNotifier.Notify(model);

            return new ActionCommandResponseResult<JournalListItem>
            {
                Response = model
            };
        }
    }
}