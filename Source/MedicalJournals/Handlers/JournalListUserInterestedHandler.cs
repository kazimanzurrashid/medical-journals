﻿namespace MedicalJournals.Handlers
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using DomainObjects;
    using Infrastructure;
    using Models;

    public class JournalListUserInterestedHandler :
        IAsyncCommandHandler<EmptyCommand, IEnumerable<InterestedJournalListItem>>
    {
        private readonly ICurrentUserProvider currentUserProvider;
        private readonly IDataContext dataContext;

        public JournalListUserInterestedHandler(
            ICurrentUserProvider currentUserProvider, 
            IDataContext dataContext)
        {
            this.currentUserProvider = currentUserProvider;
            this.dataContext = dataContext;
        }

        public async Task<IEnumerable<InterestedJournalListItem>> HandleAsync(
            EmptyCommand command,
            CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var currentUser = await this.currentUserProvider
                .GetAsync(cancellationToken);

            var journals = await this.dataContext
                .Get<Journal>()
                .Where(j => j.AvailableAt != null)
                .Where(j => j.Interests.Any(i => i.UserId == currentUser.Id))
                .Select(j => new
                {
                    j.Id,
                    j.Name,
                    j.UploadedBy.Email,
                    j.Location,
                    j.TotalPages,
                    j.Interests.FirstOrDefault(i => 
                        i.UserId == currentUser.Id).CreatedAt
                })
                .OrderByDescending(x => x.CreatedAt)
                .ToListAsync(cancellationToken);

            var list = journals.Select(x => 
                new InterestedJournalListItem
                {
                    Id = x.Id,
                    Name = x.Name,
                    Publisher = x.Email,
                    Url = ExtractRoot(x.Location),
                    TotalPages = x.TotalPages.GetValueOrDefault()
                })
            .ToList();

            return list;
        }

        private static string ExtractRoot(string location)
        {
            var index = location
                .LastIndexOf("/", StringComparison.Ordinal);

            return location.Substring(0, index);
        }
    }
}