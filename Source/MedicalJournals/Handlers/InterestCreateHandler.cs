namespace MedicalJournals.Handlers
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Validation;
    using System.Threading;
    using System.Threading.Tasks;

    using DomainObjects;
    using ExtensionMethods;
    using Infrastructure;
    using Models;

    public class InterestCreateHandler : 
        IAsyncCommandHandler<InterestCreateCommand, ActionCommandResult>
    {
        private readonly ICurrentUserProvider currentUserProvider;
        private readonly IDataContext dataContext;

        public InterestCreateHandler(
            ICurrentUserProvider currentUserProvider,
            IDataContext dataContext)
        {
            this.currentUserProvider = currentUserProvider;
            this.dataContext = dataContext;
        }

        public async Task<ActionCommandResult> HandleAsync(
            InterestCreateCommand command,
            CancellationToken cancellationToken)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            cancellationToken.ThrowIfCancellationRequested();

            if (!(await this.dataContext
                .Get<Journal>().AnyAsync(
                j => 
                    j.Id == command.JournalId &&
                    j.AvailableAt != null,
                cancellationToken)))
            {
                return null;
            }

            var currentUser = await this.currentUserProvider
                .GetAsync(cancellationToken);

            if (await this.dataContext
                .Get<Interest>()
                .AnyAsync(
                    i =>
                    i.JournalId == command.JournalId &&
                    i.UserId == currentUser.Id,
                cancellationToken))
            {
                return ActionCommandResult.Success;
            }

            var interest = new Interest
            {
                JournalId = command.JournalId,
                UserId = currentUser.Id,
                CreatedAt = Clock.Now()
            };

            this.dataContext.Get<Interest>().Add(interest);

            try
            {
                await this.dataContext.SaveChangesAsync(cancellationToken);
            }
            catch (DbEntityValidationException e)
            {
                return new ActionCommandResult()
                    .CopyErrorsFrom(e.EntityValidationErrors);
            }

            return ActionCommandResult.Success;
        }
    }
}