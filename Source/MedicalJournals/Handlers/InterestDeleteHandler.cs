namespace MedicalJournals.Handlers
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Validation;
    using System.Threading;
    using System.Threading.Tasks;

    using DomainObjects;
    using ExtensionMethods;
    using Infrastructure;
    using Models;

    public class InterestDeleteHandler : 
        IAsyncCommandHandler<InterestDeleteCommand, ActionCommandResult>
    {
        private readonly ICurrentUserProvider currentUserProvider;
        private readonly IDataContext dataContext;

        public InterestDeleteHandler(
            ICurrentUserProvider currentUserProvider,
            IDataContext dataContext)
        {
            this.currentUserProvider = currentUserProvider;
            this.dataContext = dataContext;
        }

        public async Task<ActionCommandResult> HandleAsync(
            InterestDeleteCommand command,
            CancellationToken cancellationToken)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            cancellationToken.ThrowIfCancellationRequested();

            var currentUser = await this.currentUserProvider
                .GetAsync(cancellationToken);

            var set = this.dataContext.Get<Interest>();

            var interest = await set.FirstOrDefaultAsync(
                i => i.JournalId == command.JournalId && 
                i.UserId == currentUser.Id,
                cancellationToken);

            if (interest == null)
            {
                return null;
            }

            set.Remove(interest);

            try
            {
                await this.dataContext.SaveChangesAsync(cancellationToken);
            }
            catch (DbEntityValidationException e)
            {
                return new ActionCommandResult()
                    .CopyErrorsFrom(e.EntityValidationErrors);
            }

            return ActionCommandResult.Success;
        }
    }
}