namespace MedicalJournals.Handlers
{
    using System;
    using System.Data.Entity;
    using System.IO;
    using System.IO.Abstractions;
    using System.Threading;
    using System.Threading.Tasks;

    using Serilog;

    using DomainObjects;
    using ExtensionMethods;
    using Infrastructure;
    using Models;

    public class JournalPdfToImageConvertHandler :
        IAsyncCommandHandler<JournalPdfToImageConvertCommand, EmptyCommandResult>
    {
        public const string JobName = "PdfToImage";

        private readonly IDataContext dataContext;
        private readonly IBlobStorage blobStorage;
        private readonly IPdfDocumentFactory pdfDocumentFactory;
        private readonly IBackgroundJobNotificationPusher backgroundJobNotificationPusher;
        private readonly IFileSystem fileSystem;
        private readonly ILogger logger;

        public JournalPdfToImageConvertHandler(
            IDataContext dataContext, 
            IBlobStorage blobStorage, 
            IPdfDocumentFactory pdfDocumentFactory, 
            IBackgroundJobNotificationPusher backgroundJobNotificationPusher,
            IFileSystem fileSystem,
            ILogger logger)
        {
            this.dataContext = dataContext;
            this.blobStorage = blobStorage;
            this.pdfDocumentFactory = pdfDocumentFactory;
            this.backgroundJobNotificationPusher = backgroundJobNotificationPusher;
            this.fileSystem = fileSystem;
            this.logger = logger;
        }

        public async Task<EmptyCommandResult> HandleAsync(
            JournalPdfToImageConvertCommand command,
            CancellationToken cancellationToken)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            cancellationToken.ThrowIfCancellationRequested();

            var journal = await this.GetJournalAsync(
                command.JournalId,
                cancellationToken);

            if (journal == null)
            {
                return EmptyCommandResult.Instance;
            }

            var temporaryLocation = this.CreateTemporaryDirectory(journal.Id);

            this.logger.Information(
                "Created temporary directory {TemporaryLocation} for " +
                "conversion.",
                temporaryLocation);

            var pdfFileLocation = await this.DownloadPdfAsync(
                journal,
                temporaryLocation,
                cancellationToken);

            this.logger.Information(
                "Downloaded pdf file in {TemporaryPdfLocation}.", 
                pdfFileLocation);

            ConvertionState state;

            using (var pdfDocument = this.pdfDocumentFactory
                .Get(pdfFileLocation))
            {
                state = journal.ConversionState
                    .FromServerJson<ConvertionState>() ??
                    new ConvertionState
                    {
                        TotalPages = pdfDocument.TotalPages
                    };

                if (state.ProcessedPages == 0)
                {
                    await this.backgroundJobNotificationPusher
                        .PushAsync(
                            JobName,
                            "started",
                            new
                                {
                                    id = journal.Id,
                                    processedPages = state.ProcessedPages,
                                    totalPages = state.TotalPages
                                });

                    this.logger.Information(
                        "Sent job started notification for journal {@Journal}.",
                        new { id = journal.Id, name = journal.Name });
                }

                while (state.ProcessedPages < state.TotalPages)
                {
                    state.ProcessedPages++;

                    await this.GenerateAndUploadImageAsync(
                        pdfDocument, 
                        state.ProcessedPages, 
                        journal.Id, 
                        temporaryLocation, 
                        cancellationToken);

                    if (state.ProcessedPages % 5 != 0)
                    {
                        continue;
                    }

                    journal.ConversionState = state.ToServerJson();
                    await this.dataContext.SaveChangesAsync(cancellationToken);

                    await this.backgroundJobNotificationPusher
                        .PushAsync(
                            JobName,
                            "processing", 
                            new
                                {
                                    id = journal.Id,
                                    processedPages = state.ProcessedPages,
                                    totalPages = state.TotalPages
                                });

                    this.logger.Information(
                        "Saved state and sent job processing notification " +
                        "for journal {@Journal}.",
                        new
                            {
                                id = journal.Id,
                                name = journal.Name,
                                processed = state.ProcessedPages,
                                total = state.TotalPages
                            });
                }
            }

            this.logger.Information(
                "Deleted temporary directory {TemporaryLocation}.",
                temporaryLocation);

            this.DeleteDirectory(temporaryLocation);

            await this.MarkAsCompletedAsync(
                journal, 
                state.TotalPages, 
                cancellationToken);

            await this.backgroundJobNotificationPusher
                .PushAsync(
                    JobName,
                    "completed",
                    new
                        {
                            id = journal.Id,
                            timestamp = Clock.Now()
                        });

            this.logger.Information(
                "Sent job completed notification for {@Journal}.",
                new { id = journal.Id, name = journal.Name });

            return EmptyCommandResult.Instance;
        }

        private static Stream OpenFileStreamAsAsync(string location)
        {
            return new FileStream(
                location,
                FileMode.OpenOrCreate,
                FileAccess.ReadWrite,
                FileShare.None,
                8192,
                FileOptions.Asynchronous);
        }

        private async Task MarkAsCompletedAsync(
            Journal journal, 
            int totalPages, 
            CancellationToken cancellationToken)
        {
            journal.TotalPages = totalPages;
            journal.ConversionState = null;
            journal.AvailableAt = Clock.Now();

            await this.dataContext.SaveChangesAsync(cancellationToken);
        }

        private async Task<Journal> GetJournalAsync(
            string journalId,
            CancellationToken cancellationToken)
        {
            var journal = await this.dataContext
                .Get<Journal>()
                .FirstOrDefaultAsync(
                    j =>
                        j.Id == journalId &&
                        j.AvailableAt == null,
                    cancellationToken);

            return journal;
        }

        private async Task GenerateAndUploadImageAsync(
            IPdfDocument pdfDocument, 
            int pageNumber, 
            string journalId,
            string targetLocation, 
            CancellationToken cancellationToken)
        {
            var imageFileName = pageNumber + ".jpg";

            var imageFilePath = this.fileSystem.Path.Combine(
                targetLocation,
                imageFileName);

            pdfDocument.MoveToPage(pageNumber);
            pdfDocument.TakeSnapshot(imageFilePath);

            using (var image = OpenFileStreamAsAsync(imageFilePath))
            {
                var blobName = journalId +
                               this.fileSystem.Path.AltDirectorySeparatorChar +
                               imageFileName;

                await this.blobStorage
                    .UploadAsync(blobName, image, cancellationToken);
            }
        }

        private string CreateTemporaryDirectory(string journalId)
        {
            var directory = this.fileSystem
                .Path
                .Combine(this.fileSystem.Path.GetTempPath(), journalId);

            if (this.fileSystem.Directory.Exists(directory))
            {
                this.DeleteDirectory(directory);
            }

            if (!this.fileSystem.Directory.Exists(directory))
            {
                this.fileSystem.Directory.CreateDirectory(directory);
            }

            return directory;
        }

        private void DeleteDirectory(
            string path)
        {
            try
            {
                this.fileSystem.Directory.Delete(path, true);
            }
            catch (IOException)
            {
            }
        }

        private async Task<string> DownloadPdfAsync(
            Journal journal,
            string temporaryLocation,
            CancellationToken cancellationToken)
        {
            var temporaryFileName = journal.Name + ".pdf";
            var temporaryFileLocation = this.fileSystem
                .Path
                .Combine(temporaryLocation, temporaryFileName);

            using (var target = OpenFileStreamAsAsync(temporaryFileLocation))
            {
                await this.blobStorage.LoadContentAsync(
                    journal.Location,
                    target,
                    cancellationToken);
            }

            return temporaryFileLocation;
        }

        private class ConvertionState
        {
            public int TotalPages { get; set; }

            public int ProcessedPages { get; set; }
        }
    }
}