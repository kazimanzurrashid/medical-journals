namespace MedicalJournals.Infrastructure
{
    using System;
    using System.Threading.Tasks;

    using Microsoft.AspNet.SignalR.Client;

    using Serilog;

    public interface IBackgroundJobNotificationPusher : IDisposable
    {
        Task PushAsync(string jobName, string eventName, object model);
    }

    [CLSCompliant(false)]
    public class BackgroundJobNotificationPusher :
        Disposable, IBackgroundJobNotificationPusher
    {
        private readonly HubConnection connection;
        private readonly IHubProxy proxy;
        private readonly ILogger logger;

        public BackgroundJobNotificationPusher(string endpoint, ILogger logger)
        {
            this.logger = logger;
            this.connection = HubConnectionFactory(endpoint);
            this.proxy = HubProxyFactory(
                this.connection,
                "BackgroundJobNotificationsHub");
        }

        public static Func<string, HubConnection> HubConnectionFactory { get; set; }
            = endpoint => new HubConnection(endpoint);

        public static Func<HubConnection, string, IHubProxy> HubProxyFactory { get; set; }
            = (connection, name) => connection.CreateHubProxy(name);

        public async Task PushAsync(
            string jobName,
            string eventName,
            object model)
        {
            if (!await this.EnsureConnectedAsync())
            {
                return;
            }

            try
            {
                await this.proxy.Invoke("Push", jobName, eventName, model );
            }
            catch (Exception e)
            {
                this.logger.Error(
                    e,
                    "The following exception was occurred while pushing " +
                    "notification {@Command}.",
                    new { jobName, eventName, model });
            }
        }

        protected override void DisposeCore()
        {
            this.connection.Dispose();
            base.DisposeCore();
        }

        private async Task<bool> EnsureConnectedAsync()
        {
            if (this.connection.State == ConnectionState.Connected)
            {
                return true;
            }

            try
            {
                if (this.connection.State == ConnectionState.Disconnected)
                {
                    await this.connection.Start();

                    return true;
                }
            }
            catch (Exception e)
            {
                this.logger.Error(
                    e,
                    "The following exception was occurred while connecting to {HubUrl}.",
                    this.connection.Url);
            }

            return false;
        }
    }
}