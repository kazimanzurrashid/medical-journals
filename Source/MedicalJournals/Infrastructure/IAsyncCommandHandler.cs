﻿namespace MedicalJournals.Infrastructure
{
    using System.Threading;
    using System.Threading.Tasks;

    public interface IAsyncCommandHandler<in TCommand, TResult>
    {
        Task<TResult> HandleAsync(
            TCommand command,
            CancellationToken cancellationToken);
    }
}