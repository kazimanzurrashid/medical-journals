namespace MedicalJournals.Infrastructure
{
    using System;

    public static class Clock
    {
        public static Func<DateTime> Now { get; set; } 
            = () => DateTime.UtcNow;

        public static void Reset()
        {
            Now = () => DateTime.UtcNow;
        }
    }
}