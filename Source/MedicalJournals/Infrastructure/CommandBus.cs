﻿namespace MedicalJournals.Infrastructure
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    using Serilog;
    using Serilog.Events;

    using ExtensionMethods;

    public interface ICommandBus
    {
        Task<TResult> SendAsync<TResult>(
            object command,
            CancellationToken cancellationToken);
    }

    public class CommandBus : ICommandBus
    {
        private static readonly TimeSpan ExecutionWarningTime =
            TimeSpan.FromSeconds(3);

        private static readonly Type GenericAsyncCommandHandlerType =
            typeof(IAsyncCommandHandler<,>);

        private readonly Func<Type, object> resolver;
        private readonly ILogger logger;

        public CommandBus(
            Func<Type, object> resolver,
            ILogger logger)
        {
            this.resolver = resolver;
            this.logger = logger;
        }

        public async Task<TResult> SendAsync<TResult>(
            object command,
            CancellationToken cancellationToken)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            cancellationToken.ThrowIfCancellationRequested();

            var handlerType = GenericAsyncCommandHandlerType.MakeGenericType(
                command.GetType(),
                typeof(TResult));

            dynamic handler = this.resolver(handlerType);

            if (handler == null)
            {
                throw new InvalidOperationException(
                    $"Unable to find any async handler for command \"${command.GetType().ReadableName()}\"!");
            }

            var handlerName = ((object)handler).GetType()
                .ReadableName();

            using (this.logger.BeginTimedOperation(
                "CommandBus.SendAsync",
                handlerName,
                LogEventLevel.Debug,
                ExecutionWarningTime))
            {
                var result = await handler.HandleAsync(
                    (dynamic)command,
                    cancellationToken);

                this.logger.Information(
                    "{@Command} handled by {Handler} and returns {@Result}",
                    command,
                    handlerName,
                    result);

                return result;
            }
        }
    }
}