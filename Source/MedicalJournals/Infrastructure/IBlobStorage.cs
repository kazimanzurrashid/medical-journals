﻿namespace MedicalJournals.Infrastructure
{
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;

    public interface IBlobStorage
    {
        Task<string> UploadAsync(
            string name, 
            Stream content, 
            CancellationToken cancellationToken);

        Task LoadContentAsync(
            string source,
            Stream target,
            CancellationToken cancellationToken);
    }
}