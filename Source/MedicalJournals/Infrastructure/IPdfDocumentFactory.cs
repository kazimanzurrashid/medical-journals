namespace MedicalJournals.Infrastructure
{
    public interface IPdfDocumentFactory
    {
        IPdfDocument Get(string source);
    }
}