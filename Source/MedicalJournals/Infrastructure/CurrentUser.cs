﻿namespace MedicalJournals.Infrastructure
{
    public class CurrentUser
    {
        public CurrentUser(string id, string name, bool isPublisher)
        {
            this.Id = id;
            this.Name = name;
            this.IsPublisher = isPublisher;
        }

        public string Id { get; }

        public string Name { get; }

        public bool IsPublisher { get; }
    }
}