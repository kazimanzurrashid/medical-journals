namespace MedicalJournals.Infrastructure
{
    using System.Threading.Tasks;

    using Models;

    public interface INewJournalUploadNotifier
    {
        Task Notify(JournalListItem journal);
    }
}