namespace MedicalJournals.Infrastructure
{
    using System.Threading;
    using System.Threading.Tasks;

    public interface IJobQueue
    {
        Task EnqueueAsync<TCommand>(
            TCommand command,
            CancellationToken cancellationToken) 
            where TCommand : class;
    }
}