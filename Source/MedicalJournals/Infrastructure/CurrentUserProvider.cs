﻿namespace MedicalJournals.Infrastructure
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Security.Principal;
    using System.Threading;
    using System.Threading.Tasks;

    using DomainObjects;

    public interface ICurrentUserProvider
    {
        Task<CurrentUser> GetAsync(CancellationToken cancellationToken);
    }

    public class CurrentUserProvider : ICurrentUserProvider
    {
        private readonly Func<IDataContext> lazyDataContext;

        private CurrentUser cache;

        public CurrentUserProvider(Func<IDataContext> lazyDataContext)
        {
            this.lazyDataContext = lazyDataContext;
        }

        public virtual IIdentity CurrentIdentity =>
            Thread.CurrentPrincipal.Identity;

        public async Task<CurrentUser> GetAsync(CancellationToken cancellationToken)
        {
            if (this.cache != null)
            {
                return this.cache;
            }

            this.cache = await this.LoadAsync(cancellationToken);

            return this.cache;
        }

        private async Task<CurrentUser> LoadAsync(
            CancellationToken cancellationToken)
        {
            var identity = this.CurrentIdentity;

            if (identity == null || !identity.IsAuthenticated)
            {
                return null;
            }

            var dataContext = this.lazyDataContext();

            var user = await dataContext
                .Get<User>()
                .Select(u => new
                {
                    u.Id,
                    u.UserName,
                    u.IsPublisher
                })
                .FirstOrDefaultAsync(
                    u => u.UserName == identity.Name,
                    cancellationToken);

            return user == null
                ? null
                : new CurrentUser(user.Id, user.UserName, user.IsPublisher);
        }
    }
}