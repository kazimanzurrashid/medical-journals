namespace MedicalJournals.Infrastructure
{
    using System;

    public interface IPdfDocument : IDisposable
    {
        int TotalPages { get; }

        void MoveToPage(int pageNumber);

        void TakeSnapshot(string target);
    }
}