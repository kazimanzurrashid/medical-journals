﻿namespace MedicalJournals.Infrastructure
{
    using System.Configuration;
    using System.Data.Entity;
    using System.Threading;
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity.EntityFramework;

    using DomainObjects;

    public interface IDataContext
    {
        IDbSet<TEntity> Get<TEntity>() where TEntity : class;

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }

    public class DataContext : IdentityDbContext<User>, IDataContext
    {
        public DataContext() : 
            this(ConfigurationManager.AppSettings["app.database"])
        {
        }

        public DataContext(string nameOrConnectionString) 
            : base(nameOrConnectionString)
        {
        }

        public IDbSet<Journal> Journals => this.Get<Journal>();

        public IDbSet<Interest> Interests => this.Get<Interest>();

        public virtual IDbSet<TEntity> Get<TEntity>() where TEntity : class
        {
            return this.Set<TEntity>();
        }
    }
}