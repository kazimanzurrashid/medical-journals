namespace MedicalJournals.Infrastructure
{
    using System.Threading;
    using System.Threading.Tasks;

    public interface IJobProcessor
    {
        Task ProcessAsync(CancellationToken cancellationToken);
    }
}