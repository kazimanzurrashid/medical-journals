namespace MedicalJournals.Infrastructure
{
    using System;
    using System.Configuration;
    using System.IO.Abstractions;
    using System.Linq;

    using Autofac;

    using ExtensionMethods;

    public class ServiceRegistrations : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            builder.Register<Func<Type, object>>(
                c =>
                {
                    var ctx = c.Resolve<IComponentContext>();
                    return ctx.Resolve;
                });

            builder.RegisterInstance(AssembliesProvider.Instance)
                .AsImplementedInterfaces()
                .AsSelf();

            builder.RegisterClosingTypes(
                typeof(IAsyncCommandHandler<,>),
                AssembliesProvider.Instance.Assemblies.ToArray());

            builder.Register<CommandBus>();
            builder.Register<BackgroundJobNotificationPusher>()
                .WithParameter(
                    "endpoint",
                    ConfigurationManager.AppSettings["app.url"]);

            builder.Register<FileSystem>();
        }
    }
}