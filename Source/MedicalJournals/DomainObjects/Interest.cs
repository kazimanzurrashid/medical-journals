﻿namespace MedicalJournals.DomainObjects
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("interests")]
    public class Interest
    {
        [Key, Column(Order = 1)]
        public string JournalId { get; set; }

        [Key, Column(Order = 2)]
        public string UserId { get; set; }

        [ForeignKey("JournalId")]
        public virtual Journal Journal { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}