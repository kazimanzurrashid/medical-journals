﻿namespace MedicalJournals.DomainObjects
{
    using System.Collections.Generic;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class User : IdentityUser
    {
        private ICollection<Journal> uploads;
        private ICollection<Interest> interests;

        public bool IsPublisher { get; set; }

        public virtual ICollection<Journal> Uploads => 
            this.uploads ?? (this.uploads = new HashSet<Journal>());

        public virtual ICollection<Interest> Interests => 
            this.interests ?? (this.interests = new HashSet<Interest>());
    }
}