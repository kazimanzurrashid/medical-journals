﻿namespace MedicalJournals.DomainObjects
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("journals")]
    public class Journal
    {
        private ICollection<Interest> interests;

        [Key, StringLength(36)]
        public string Id { get; set; }

        [Required, StringLength(384)]
        public string Name { get; set; }

        [Required, StringLength(2048)]
        public string Location { get; set; }

        [Required]
        public string UploadById { get; set; }

        [ForeignKey("UploadById")]
        public virtual User UploadedBy { get; set; }

        public DateTime UploadedAt { get; set; }

        public DateTime? AvailableAt { get; set; }

        public int? TotalPages { get; set; }

        [StringLength(4096)]
        public string ConversionState { get; set; }

        public virtual ICollection<Interest> Interests => 
            this.interests ?? (this.interests = new HashSet<Interest>());
    }
}