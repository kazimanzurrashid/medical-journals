﻿namespace MedicalJournals.AmazonCloud
{
    using System;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;

    using Amazon.S3;
    using Amazon.S3.Transfer;

    using Infrastructure;

    using Serilog;

    public class BlobStorage : IBlobStorage
    {
        private readonly string bucketName;
        private readonly TimeSpan cacheDuration;
        private readonly IAmazonS3 client;
        private readonly ILogger logger;

        public BlobStorage(
            string bucketName, 
            TimeSpan cacheDuration, 
            IAmazonS3 client,
            ILogger logger)
        {
            this.bucketName = bucketName;
            this.cacheDuration = cacheDuration;
            this.client = client;
            this.logger = logger;
        }

        public async Task<string> UploadAsync(
            string name,
            Stream content,
            CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException(nameof(name));
            }

            if (content == null)
            {
                throw new ArgumentNullException(nameof(content));
            }

            using (var uploader = new TransferUtility(this.client))
            {
                var request = new TransferUtilityUploadRequest
                {
                    AutoResetStreamPosition = false,
                    BucketName = this.bucketName,
                    Key = name,
                    InputStream = content,
                    ContentType = MimeMapping.GetMimeMapping(name),
                    CannedACL = S3CannedACL.PublicRead,
                    Headers =
                    {
                        CacheControl = $"private; max-age={Convert.ToInt64(this.cacheDuration.TotalSeconds)}"
                    }
                };

                try
                {
                    await uploader.UploadAsync(request, cancellationToken)
                        .ConfigureAwait(false);

                    return $"https://s3.amazonaws.com/{this.bucketName}/{name}";
                }
                catch (Exception e)
                {
                    this.logger.Error(
                        e,
                        "The following exception has occurred while " +
                        "uploading {File}:",
                        name);
                    throw;
                }
            }
        }
        
        public async Task LoadContentAsync(
            string source,
            Stream target,
            CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                throw new ArgumentException(nameof(source));
            }

            if (target == null)
            {
                throw new ArgumentNullException(nameof(target));
            }

            var prefix = $"https://s3.amazonaws.com/{this.bucketName}/";
            var key = source.Substring(prefix.Length);

            using (var downloader = new TransferUtility(this.client))
            {
                var request = new TransferUtilityOpenStreamRequest
                                  {
                                      BucketName = this.bucketName,
                                      Key = key
                                  };
                Stream stream;

                try
                {
                    stream = await downloader.OpenStreamAsync(
                        request, 
                        cancellationToken)
                        .ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    this.logger.Error(
                        e,
                        "The following exception has occurred while " +
                        "opening the source {File}:",
                        source);

                    throw;
                }

                try
                {
                    await stream.CopyToAsync(target, 8192, cancellationToken)
                        .ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    this.logger.Error(
                        e,
                        "The following exception has occurred while " +
                        "copying the source {File}:",
                        source);

                    throw;
                    
                }
                finally
                {
                    stream.Close();
                }
            }
        }
    }
}