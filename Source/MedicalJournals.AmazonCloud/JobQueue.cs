﻿namespace MedicalJournals.AmazonCloud
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    using Amazon.SQS;
    using Amazon.SQS.Model;

    using Serilog;

    using ExtensionMethods;
    using Infrastructure;

    public class JobQueue : IJobQueue
    {
        private readonly string name;
        private readonly IAmazonSQS client;
        private readonly ILogger logger;

        private string url;

        public JobQueue(string name, IAmazonSQS client, ILogger logger)
        {
            this.name = name;
            this.client = client;
            this.logger = logger;
        }

        public async Task EnqueueAsync<TCommand>(
            TCommand command,
            CancellationToken cancellationToken) where TCommand : class
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            cancellationToken.ThrowIfCancellationRequested();

            var queueUrl = await this.GetQueueUrlAsync(cancellationToken)
                .ConfigureAwait(false);

            cancellationToken.ThrowIfCancellationRequested();

            var request = new SendMessageRequest
                              {
                                  MessageBody = command.ToServerJson(),
                                  QueueUrl = queueUrl
                              };

            request.MessageAttributes.Add(
                "CommandType",
                new MessageAttributeValue
                {
                    DataType = "String",
                    StringValue = command.GetType().AssemblyQualifiedName
                });

            var response = await this.client.SendMessageAsync(
                request,
                cancellationToken)
                .ConfigureAwait(false);

            var job = new { response.MessageId, Command = command };

            this.logger.Information("Job enqueued {@Job}.", job);
        }

        private async Task<string> GetQueueUrlAsync(
            CancellationToken cancellationToken)
        {
            if (!string.IsNullOrWhiteSpace(this.url))
            {
                return this.url;
            }

            var request = new GetQueueUrlRequest(this.name);
            var response = await this.client.GetQueueUrlAsync(
                request,
                cancellationToken)
                .ConfigureAwait(false);

            this.url = response.QueueUrl;

            return this.url;
        }
    }
}