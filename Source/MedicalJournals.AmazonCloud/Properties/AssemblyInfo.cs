﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("MedicalJournals.AmazonCloud")]
[assembly: AssemblyProduct("MedicalJournals.AmazonCloud")]
[assembly: CLSCompliant(true)]
[assembly: Guid("9b323708-10b7-4d66-a584-a1ca07ebab10")]
[assembly: NeutralResourcesLanguage("en-US")]