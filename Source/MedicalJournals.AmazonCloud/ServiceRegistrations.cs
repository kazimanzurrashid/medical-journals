namespace MedicalJournals.Infrastructure
{
    using System;
    using System.Configuration;
    using System.Globalization;

    using Amazon.S3;
    using Amazon.SQS;

    using Autofac;

    using AmazonCloud;
    using ExtensionMethods;

    public class ServiceRegistrations : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            RegisterBlobStorage(builder);
            RegisterJobQueue(builder);
        }

        private static void RegisterBlobStorage(ContainerBuilder builder)
        {
            builder.Register<AmazonS3Client>();

            var bucketName = ConfigurationManager
                .AppSettings["aws.s3.bucket"];

            var cacheDuration = TimeSpan.Parse(
                ConfigurationManager.AppSettings["aws.s3.cacheDuration"],
                CultureInfo.CurrentCulture);

            builder.Register<BlobStorage>()
                .WithParameter("bucketName", bucketName)
                .WithParameter("cacheDuration", cacheDuration);
        }

        private static void RegisterJobQueue(ContainerBuilder builder)
        {
            builder.Register<AmazonSQSClient>();

            var queueName = ConfigurationManager
                .AppSettings["aws.sqs.name"];

            var maxNumberOfConcurrentJobs = Convert.ToInt32(
                ConfigurationManager
                .AppSettings["aws.sqs.maxNumberOfConcurrentJobs"],
                CultureInfo.CurrentCulture);

            builder.Register<JobQueue>().WithParameter("name", queueName);

            builder.Register<JobProcessor>()
                .WithParameter("name", queueName)
                .WithParameter(
                    "maxNumberOfConcurrentJobs",
                    maxNumberOfConcurrentJobs);
        }
    }
}