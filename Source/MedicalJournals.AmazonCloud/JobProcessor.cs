﻿namespace MedicalJournals.AmazonCloud
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using Amazon.SQS;
    using Amazon.SQS.Model;

    using Serilog;

    using ExtensionMethods;
    using Infrastructure;

    public class JobProcessor : IJobProcessor
    {
        private const int MaxNumberOfMessagesToReceive = 10;

        private readonly string name;
        private readonly int maxNumberOfConcurrentJobs;
        private readonly IAmazonSQS client;
        private readonly ICommandBus commandBus;
        private readonly ILogger logger;

        private int activeTasksCount;

        public JobProcessor(
            string name,
            int maxNumberOfConcurrentJobs, 
            IAmazonSQS client, 
            ICommandBus commandBus,
            ILogger logger)
        {
            this.name = name;
            this.maxNumberOfConcurrentJobs = maxNumberOfConcurrentJobs;
            this.client = client;
            this.commandBus = commandBus;
            this.logger = logger;
        }

        public async Task ProcessAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var queueUrl = await this.GetQueueUrlAsync(cancellationToken);

            while (!cancellationToken.IsCancellationRequested)
            {
                if (this.activeTasksCount >= this.maxNumberOfConcurrentJobs)
                {
                    this.logger.Warning(
                        "Already executing {MaxNumberOfConcurrentJobs} which" +
                        " is the maximum number of concurrent jobs.", 
                        this.maxNumberOfConcurrentJobs);

                    continue;
                }

                this.logger.Debug("Waiting for jobs.");

                var receiveRequest = new ReceiveMessageRequest
                {
                    QueueUrl = queueUrl,
                    MessageAttributeNames = new List<string> { "CommandType" },
                    MaxNumberOfMessages = this.CalculateMaxNumberOfMessages()
                };

                var receiveResponse = await this.client.ReceiveMessageAsync(
                    receiveRequest,
                    cancellationToken)
                    .ConfigureAwait(false);

                if (!receiveResponse.Messages.Any())
                {
                    continue;
                }

                this.logger.Information(
                    "Received {JobCount} jobs.",
                    receiveResponse.Messages.Count);

                foreach (var message in receiveResponse.Messages)
                {
                    var command = BuildCommandFrom(message);

                    this.HandleCommand(
                        command,
                        message,
                        queueUrl,
                        cancellationToken);

                    Interlocked.Increment(ref this.activeTasksCount);
                }
            }
        }

        private static object BuildCommandFrom(Message message)
        {
            var commandTypeName = message.MessageAttributes["CommandType"].StringValue;
            var commandType = Type.GetType(commandTypeName, true);
            var command = message.Body.FromServerJson(commandType);

            return command;
        }

        private void HandleCommand(
            object command, 
            Message message, 
            string queueUrl,
            CancellationToken cancellationToken)
        {
            this.commandBus
                .SendAsync(command, cancellationToken)
                .ContinueWith(
                    task =>
                    {
                        Interlocked.Decrement(ref this.activeTasksCount);

                        if (!task.IsCanceled && !task.IsFaulted)
                        {
                            this.client.DeleteMessageAsync(
                                queueUrl, 
                                message.ReceiptHandle,
                                cancellationToken)
                                .ConfigureAwait(false);

                            this.logger.Information(
                                "Completed processing of command {@Command}.",
                                command);
                            return;
                        }

                        if (task.IsFaulted && task.Exception != null)
                        {
                            this.logger.Error(
                                task.Exception.GetBaseException(),
                                "The following exception has occurred while handling job {@Job}.",
                                message);

                            return;
                        }

                        if (task.IsCanceled)
                        {
                            this.logger.Warning("The job {@Job} was canceled.", message);
                        }
                    },
                    cancellationToken)
                .ConfigureAwait(false);
        }

        private int CalculateMaxNumberOfMessages()
        {
            var remaining = this.maxNumberOfConcurrentJobs 
                - this.activeTasksCount;

            if (remaining > MaxNumberOfMessagesToReceive)
            {
                return MaxNumberOfMessagesToReceive;
            }

            return remaining < 0 ? 1 : remaining;
        }

        private async Task<string> GetQueueUrlAsync(
            CancellationToken cancellationToken)
        {
            var request = new GetQueueUrlRequest(this.name);
            var response = await this.client.GetQueueUrlAsync(
                request,
                cancellationToken)
                .ConfigureAwait(false);

            return response.QueueUrl;
        }
    }
}