﻿namespace MedicalJournals.Server.Specs.Handlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using Machine.Specifications;
    using NSubstitute;

    using DomainObjects;
    using Infrastructure;
    using MedicalJournals.Handlers;
    using Models;

    using Fakes;

    [Subject(typeof(JournalPagedListHandler))]
    public class On_handling_command
    {
        private Establish context = () =>
        {
            var currentUser = new CurrentUser(UserId, "Mr. Test", false);
            var currentUserProvider = Substitute.For<ICurrentUserProvider>();

            currentUserProvider.GetAsync(CancellationToken.None)
                .Returns(Task.FromResult(currentUser));

            var journal1 = new Journal
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Journal 1",
                UploadedBy = new User
                {
                    Email = "publisher@example.com"
                }
            };

            journal1.Interests.Add(new Interest
            {
                UserId = UserId
            });

            var journal2 = new Journal
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Journal 2",
                UploadedBy = new User
                {
                    Email = "publisher@example.com"
                }
            };

            journal2.Interests.Add(new Interest
            {
                UserId = Guid.NewGuid().ToString()
            });

            var journals = new List<Journal> {journal1, journal2};

            var dataContext = Substitute.For<IDataContext>();
            dataContext.Get<Journal>()
                .Returns(new FakeAsyncDbSet<Journal>(journals));

            handler = new JournalPagedListHandler(
                currentUserProvider, 
                dataContext);
        };

        Because of = () => 
            result = handler.HandleAsync(
                new PagedListCommand(), 
                CancellationToken.None).Result;

        It returns_journals_from_db_with_current_user_interest = () =>
        {
            result.Count.ShouldEqual(2);
            result.List.ShouldMatch(list => list.Count(i => i.Interested) == 1);
        };

        static PagedListResult<JournalListItem> result;
        static JournalPagedListHandler handler;

        static readonly string UserId = Guid.NewGuid().ToString();
    }
}