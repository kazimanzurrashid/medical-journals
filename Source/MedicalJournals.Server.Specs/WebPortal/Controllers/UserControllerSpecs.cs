﻿namespace MedicalJournals.Server.Specs.WebPortal.Controllers
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    using NSubstitute;
    using Machine.Specifications;

    using Infrastructure;
    using MedicalJournals.WebPortal.Controllers;
    using Models;

    [Subject(typeof(UserController))]
    public class On_visiting_sign_up
    {
        Establish context = () =>
            controller = new UserController(Substitute.For<ICommandBus>());

        Because of = () => result = (ViewResult)controller.Signup();

        It renders_default_view = () => result.ViewName.ShouldBeEmpty();

        It sets_sign_up_command_as_model = () => 
            result.Model.ShouldBeOfExactType<SignupCommand>();

        static ViewResult result;
        static UserController controller;
    }

    [Subject(typeof(UserController))]
    public class On_successful_sign_up
    {
        Establish context = () =>
        {
            var commandBus = Substitute.For<ICommandBus>();
            commandBus.SendAsync<ActionCommandResult>(
                Arg.Any<SignupCommand>(), 
                Arg.Any<CancellationToken>())
                .Returns(Task.FromResult(ActionCommandResult.Success));

            controller = new UserController(commandBus);
        };

        Because of = () =>
            result = (RedirectToRouteResult)controller.Signup(
                new SignupCommand(),
                CancellationToken.None).Result;

        It flashes_success_message = () =>
                controller.Flash[FlashMessageType.Success].ShouldNotBeEmpty();

        It redirects_to_sign_in = () =>
        {
            result.RouteValues["controller"].ShouldBeNull();
            result.RouteValues["action"].ShouldEqual("SignIn");
        };

        static RedirectToRouteResult result;
        static UserController controller;
    }

    [Subject(typeof(UserController))]
    public class on_unsuccessful_sign_up_due_to_validation_issue
    {
        Establish context = () =>
            {
                controller = new UserController(Substitute.For<ICommandBus>());
                controller.ModelState.AddModelError("Email", "Email is required");
            };

        Because of = () =>
            result = (ViewResult)controller.Signup(
                command,
                CancellationToken.None).Result;

        It renders_same_view = () => 
            result.ViewName.ShouldBeEmpty();

        It sets_same_command_as_model = () => 
            result.Model.ShouldBeTheSameAs(command);

        static SignupCommand command;
        static ViewResult result;
        static UserController controller;
    }

    [Subject(typeof(UserController))]
    public class On_unsuccessful_sign_up_due_to_invalid_credentials
    {
        Establish context = () =>
        {
            var commandBus = Substitute.For<ICommandBus>();
            commandBus.SendAsync<ActionCommandResult>(
                Arg.Any<SignupCommand>(), 
                Arg.Any<CancellationToken>())
                .Returns(Task.FromResult(new ActionCommandResult(ErrorMessage)));

            controller = new UserController(commandBus);
            command = new SignupCommand();
        };

        Because of = () =>
            result = (ViewResult)controller.Signup(
                command,
                CancellationToken.None).Result;

        It copies_errors_to_model_states = () =>
            controller.ModelState
                .First()
                .Value
                .Errors
                .First()
                .ErrorMessage
                .ShouldEqual(ErrorMessage);

        It renders_same_view = () => 
            result.ViewName.ShouldBeEmpty();

        It sets_same_command_as_model = () => 
            result.Model.ShouldBeTheSameAs(command);

        static SignupCommand command;
        static ViewResult result;
        static UserController controller;

        const string ErrorMessage = "Invalid credentials";
    }

    [Subject(typeof(UserController))]
    public class On_visiting_sign_in
    {
        Establish context = () =>
            controller = new UserController(Substitute.For<ICommandBus>());

        Because of = () => result = (ViewResult)controller.SignIn();

        It renders_default_view = () => result.ViewName.ShouldBeEmpty();

        static ViewResult result;
        static UserController controller;
    }
}