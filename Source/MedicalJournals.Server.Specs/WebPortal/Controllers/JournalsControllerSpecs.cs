﻿namespace MedicalJournals.Server.Specs.WebPortal.Controllers
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http.Results;

    using Machine.Specifications;
    using NSubstitute;

    using Infrastructure;
    using MedicalJournals.WebPortal.Controllers;
    using Models;

    [Subject(typeof(JournalsController))]
    public class On_getting_all_journals_with_valid_arguments
    {
        Establish context = () =>
        {
            pagedListResult = new PagedListResult<JournalListItem>
            {
                Count = 1,
                List = new List<JournalListItem>
                {
                    new JournalListItem()
                }
            };

            var commandBus = Substitute.For<ICommandBus>();
            commandBus.SendAsync<PagedListResult<JournalListItem>>(
                Arg.Any<PagedListCommand>(), 
                Arg.Any<CancellationToken>())
                .Returns(Task.FromResult(pagedListResult));

            controller = new JournalsController(commandBus);
        };

        Because of = () =>
            actionResult = (OkNegotiatedContentResult<PagedListResult<JournalListItem>>)
                controller.All(
                    new PagedListCommand(),
                    CancellationToken.None).Result;

        It returns_paged_journals = () =>
            actionResult.Content.ShouldBeTheSameAs(pagedListResult);

        static OkNegotiatedContentResult<PagedListResult<JournalListItem>> actionResult;
        static PagedListResult<JournalListItem> pagedListResult;
        static JournalsController controller;
    }

    [Subject(typeof(JournalsController))]
    public class On_getting_all_journals_with_invalid_aruments
    {
        Establish context = () =>
        {
            controller = new JournalsController(Substitute.For<ICommandBus>());
            controller.ModelState
            .AddModelError("sortOrder", "Invalid sort order");
        };

        Because of = () =>
            actionResult = (InvalidModelStateResult)
                controller.All(
                    new PagedListCommand(),
                    CancellationToken.None).Result;

        It returns_model_state_errors = () => actionResult.ShouldNotBeNull();

        static InvalidModelStateResult actionResult;
        static JournalsController controller;
    }
}