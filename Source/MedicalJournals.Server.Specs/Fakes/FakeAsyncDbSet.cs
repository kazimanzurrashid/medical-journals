﻿namespace MedicalJournals.Server.Specs.Fakes
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    public class FakeAsyncDbSet<T> : IDbSet<T> where T : class
    {
        private readonly ICollection<T> store;
        private readonly IQueryable<T> query;
 
        public FakeAsyncDbSet() : this(new List<T>())
        {
        }

        public FakeAsyncDbSet(ICollection<T> store)
        {
            this.store = store;
            this.query = store.AsQueryable();
        }

        public Expression Expression => this.query.Expression;

        public Type ElementType => this.query.ElementType;

        public IQueryProvider Provider => 
            new FakeAsyncQueryProvider<T>(this.query.Provider);

        public ObservableCollection<T> Local => 
            new ObservableCollection<T>(this.store);

        public IEnumerator<T> GetEnumerator()
        {
            return this.store.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public T Find(params object[] keyValues)
        {
            throw new NotImplementedException();
        }

        public T Add(T entity)
        {
            this.store.Add(entity);
            return entity;
        }

        public T Remove(T entity)
        {
            this.store.Remove(entity);
            return entity;
        }

        public T Attach(T entity)
        {
            return this.Add(entity);
        }

        public T Create()
        {
            return Activator.CreateInstance<T>();
        }

        public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, T
        {
            return Activator.CreateInstance<TDerivedEntity>();
        }
    }
}