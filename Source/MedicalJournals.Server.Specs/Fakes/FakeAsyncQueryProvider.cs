﻿namespace MedicalJournals.Server.Specs.Fakes
{
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading;
    using System.Threading.Tasks;

    public class FakeAsyncQueryProvider<TEntity> : IDbAsyncQueryProvider
    {
        private readonly IQueryProvider provider;

        public FakeAsyncQueryProvider(IQueryProvider provider)
        {
            this.provider = provider;
        }

        public IQueryable CreateQuery(Expression expression)
        {
            return new FakeAsyncEnumerable<TEntity>(expression); 
        }

        public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            return new FakeAsyncEnumerable<TElement>(expression); 
        }

        public object Execute(Expression expression)
        {
            return this.provider.Execute(expression);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            return this.provider.Execute<TResult>(expression);
        }

        public Task<object> ExecuteAsync(
            Expression expression,
            CancellationToken cancellationToken)
        {
            return Task.FromResult(this.Execute(expression));
        }

        public Task<TResult> ExecuteAsync<TResult>(
            Expression expression,
            CancellationToken cancellationToken)
        {
            return Task.FromResult(this.Execute<TResult>(expression));
        }
    }
}