namespace MedicalJournals.MicrosoftCloud
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    using Microsoft.WindowsAzure.Storage;
    using Microsoft.WindowsAzure.Storage.Queue;
    using Microsoft.WindowsAzure.Storage.RetryPolicies;

    using Serilog;

    using ExtensionMethods;
    using Infrastructure;

    [CLSCompliant(false)]
    public class JobProcessor : IJobProcessor
    {
        private readonly string name;
        private readonly TimeSpan timeToLive;
        private readonly int maxNumberOfConcurrentJobs;
        private readonly CloudQueueClient client;
        private readonly ICommandBus commandBus;
        private readonly ILogger logger;

        private int activeTasksCount;

        public JobProcessor(
            string name, 
            TimeSpan timeToLive,
            int maxNumberOfConcurrentJobs,
            CloudQueueClient client,
            ICommandBus commandBus,
            ILogger logger)
        {
            this.name = name;
            this.timeToLive = timeToLive;
            this.maxNumberOfConcurrentJobs = maxNumberOfConcurrentJobs;
            this.client = client;
            this.commandBus = commandBus;
            this.logger = logger;
        }

        public async Task ProcessAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            var queue = this.client.GetQueueReference(this.name);

            await queue.CreateIfNotExistsAsync(cancellationToken);

            while (!cancellationToken.IsCancellationRequested)
            {
                if (this.activeTasksCount >= this.maxNumberOfConcurrentJobs)
                {
                    this.logger.Warning(
                        "Already executing {MaxNumberOfConcurrentJobs} which" +
                        " is the maximum number of concurrent jobs.", 
                        this.maxNumberOfConcurrentJobs);

                    continue;
                }

                this.logger.Debug("Waiting for jobs.");

                var options = new QueueRequestOptions
                {
                    RetryPolicy = new ExponentialRetry()
                };

                var context = new OperationContext();

                var message = await queue.GetMessageAsync(
                    this.timeToLive,
                    options,
                    context,
                    cancellationToken);

                if (message == null)
                {
                    continue;
                }

                this.logger.Information("Received job.");

                var command = BuildCommandFrom(message);

                this.HandleCommand(
                    command,
                    message,
                    queue,
                    cancellationToken);

                Interlocked.Increment(ref this.activeTasksCount);
            }
        }

        private static object BuildCommandFrom(CloudQueueMessage message)
        {
            var payload = message.AsString
                .FromServerJson<Pair<string, string>>();

            var type = Type.GetType(payload.Key, true);
            var command = payload.Value.FromServerJson(type);

            return command;
        }

        private void HandleCommand(
            object command, 
            CloudQueueMessage message,
            CloudQueue queue,
            CancellationToken cancellationToken)
        {
            this.commandBus
                .SendAsync(command, cancellationToken)
                .ContinueWith(
                    task =>
                    {
                        Interlocked.Decrement(ref this.activeTasksCount);

                        if (!task.IsCanceled && !task.IsFaulted)
                        {
                            queue.DeleteMessageAsync(
                                message,
                                cancellationToken);

                            this.logger.Information(
                                "Completed processing of command {@Command}.",
                                command);
                            return;
                        }

                        if (task.IsFaulted && task.Exception != null)
                        {
                            this.logger.Error(
                                task.Exception.GetBaseException(),
                                "The following exception has occurred while handling job {@Job}.",
                                message);

                            return;
                        }

                        if (task.IsCanceled)
                        {
                            this.logger.Warning("The job {@Job} was canceled.", message);
                        }
                    },
                    cancellationToken);
        }
    }
}