﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("MedicalJournals.MicrosoftCloud")]
[assembly: AssemblyProduct("MedicalJournals.MicrosoftCloud")]
[assembly: CLSCompliant(true)]
[assembly: Guid("666e2d10-9b75-4973-bea2-31dbe176a1cb")]
[assembly: NeutralResourcesLanguage("en-US")]
