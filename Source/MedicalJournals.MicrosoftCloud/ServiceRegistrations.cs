namespace MedicalJournals.Infrastructure
{
    using System;
    using System.Configuration;
    using System.Globalization;

    using Microsoft.Azure;
    using Microsoft.WindowsAzure.Storage;

    using Autofac;

    using ExtensionMethods;
    using MicrosoftCloud;

    public class ServiceRegistrations : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            builder.Register(_ =>
                CloudStorageAccount.Parse(CloudConfigurationManager
                .GetSetting("azure.storage.connectionString")))
                .AsSelf()
                .AsImplementedInterfaces()
                .SingleInstance();

            RegisterBlobStorage(builder);
            RegisterJobQueue(builder);
        }

        private static void RegisterBlobStorage(ContainerBuilder builder)
        {
            builder.Register(c =>
            {
                var context = c.Resolve<IComponentContext>();
                var account = context.Resolve<CloudStorageAccount>();

                return account.CreateCloudBlobClient();
            })
            .AsSelf()
            .AsImplementedInterfaces();

            var containerName = ConfigurationManager
                .AppSettings["azure.blob.container"];

            var cacheDuration = TimeSpan.Parse(
                ConfigurationManager.AppSettings["azure.blob.cacheDuration"],
                CultureInfo.CurrentCulture);

            builder.Register<BlobStorage>()
                .WithParameter("containerName", containerName)
                .WithParameter("cacheDuration", cacheDuration);
        }

        private static void RegisterJobQueue(ContainerBuilder builder)
        {
            builder.Register(c =>
            {
                var context = c.Resolve<IComponentContext>();
                var account = context.Resolve<CloudStorageAccount>();

                return account.CreateCloudQueueClient();
            })
            .AsSelf()
            .AsImplementedInterfaces();

            var queueName = ConfigurationManager
                .AppSettings["azure.queue.name"];

            var maxNumberOfConcurrentJobs = Convert.ToInt32(
                ConfigurationManager
                .AppSettings["azure.queue.maxNumberOfConcurrentJobs"],
                CultureInfo.CurrentCulture);

            var timeToLive = TimeSpan.Parse(
                ConfigurationManager.AppSettings["azure.queue.timeToLive"],
                CultureInfo.CurrentCulture);

            builder.Register<JobQueue>().WithParameter("name", queueName);

            builder.Register<JobProcessor>()
                .WithParameter("name", queueName)
                .WithParameter("timeToLive", timeToLive)
                .WithParameter(
                    "maxNumberOfConcurrentJobs",
                    maxNumberOfConcurrentJobs);
        }
    }
}