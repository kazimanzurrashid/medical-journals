namespace MedicalJournals.MicrosoftCloud
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    using Microsoft.WindowsAzure.Storage.Queue;

    using Serilog;

    using ExtensionMethods;
    using Infrastructure;

    [CLSCompliant(false)]
    public class JobQueue : IJobQueue
    {
        private readonly string name;
        private readonly CloudQueueClient client;

        private readonly ILogger logger;

        public JobQueue(string name, CloudQueueClient client, ILogger logger)
        {
            this.name = name;
            this.client = client;
            this.logger = logger;
        }

        public async Task EnqueueAsync<TCommand>(
            TCommand command,
            CancellationToken cancellationToken) where TCommand : class
        {
            var queue = this.client.GetQueueReference(this.name);

            await queue.CreateIfNotExistsAsync(cancellationToken)
                .ConfigureAwait(false);

            var payload = new Pair<string, string>
            {
                Key = command.GetType().AssemblyQualifiedName,
                Value = command.ToServerJson()
            }.ToServerJson();

            var message = new CloudQueueMessage(payload);

            await queue.AddMessageAsync(message, cancellationToken)
                .ConfigureAwait(false);

            var job = new { message.Id, Command = command };

            this.logger.Information("Job enqueued {@Job}.", job);
        }
    }
}