﻿namespace MedicalJournals.MicrosoftCloud
{ 
    using System;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;

    using Microsoft.WindowsAzure.Storage;
    using Microsoft.WindowsAzure.Storage.Blob;
    using Microsoft.WindowsAzure.Storage.RetryPolicies;

    using Infrastructure;

    using Serilog;

    [CLSCompliant(false)]
    public class BlobStorage : IBlobStorage
    {
        private readonly string containerName;
        private readonly TimeSpan cacheDuration;
        private readonly CloudBlobClient client;
        private readonly ILogger logger;

        public BlobStorage(
            string containerName,
            TimeSpan cacheDuration,
            CloudBlobClient client,
            ILogger logger)
        {
            this.containerName = containerName;
            this.cacheDuration = cacheDuration;
            this.client = client;
            this.logger = logger;
        }

        public async Task<string> UploadAsync(
            string name, 
            Stream content, 
            CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException(nameof(name));
            }

            if (content == null)
            {
                throw new ArgumentNullException(nameof(content));
            }

            var container = await this.GetContainerAsync(cancellationToken)
                .ConfigureAwait(false);

            var file = container.GetBlockBlobReference(name);
            file.Properties.ContentType = MimeMapping.GetMimeMapping(name);
            file.Properties.CacheControl = $"private; max-age={Convert.ToInt64(this.cacheDuration.TotalSeconds)}";

            var options = new BlobRequestOptions
            {
                RetryPolicy = new ExponentialRetry()
            };

            var context = new OperationContext();

            try
            {
                await file.UploadFromStreamAsync(
                    content,
                    AccessCondition.GenerateEmptyCondition(),
                    options,
                    context,
                    cancellationToken)
                    .ConfigureAwait(false);

                return file.Uri.ToString();
            }
            catch (Exception e)
            {
                this.logger.Error(
                    e, 
                    "The following exception has occurred while uploading {File}:", 
                    name);
                throw;
            }
        }

        public async Task LoadContentAsync(
            string source,
            Stream target,
            CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                throw new ArgumentException(nameof(source));
            }

            var container = await this.GetContainerAsync(cancellationToken)
                .ConfigureAwait(false);

            var name = this.ExtractName(source);
            var blob = container.GetBlobReference(name);

            var options = new BlobRequestOptions
            {
                RetryPolicy = new ExponentialRetry()
            };

            var context = new OperationContext();

            try
            {
                await blob.DownloadToStreamAsync(
                    target, 
                    AccessCondition.GenerateEmptyCondition(),
                    options,
                    context,
                    cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (Exception e)
            {
                this.logger.Error(
                    e, 
                    "The following exception has occurred while downloading {File}:",
                    source);
                throw;
            }
        }

        private async Task<CloudBlobContainer> GetContainerAsync(
            CancellationToken cancellationToken)
        {
            var container = this.client.GetContainerReference(
                this.containerName);

            await container.CreateIfNotExistsAsync(
                BlobContainerPublicAccessType.Blob,
                null,
                null,
                cancellationToken)
                .ConfigureAwait(false);

            return container;
        }

        private string ExtractName(string location)
        {
            var index = location.IndexOf(
                this.containerName, 
                StringComparison.OrdinalIgnoreCase);

            var name = location.Substring(
                index + this.containerName.Length + 1);

            return name;
        }
    }
}