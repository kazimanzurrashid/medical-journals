namespace MedicalJournals.DocumentTools
{
    using System;
    using System.Drawing.Imaging;

    using Ghostscript.NET;
    using Ghostscript.NET.Rasterizer;

    using Serilog;

    using Infrastructure;

    public class GhostPdfDocument : Disposable, IPdfDocument
    {
        private readonly ILogger logger;
        private readonly string source;
        private readonly GhostscriptRasterizer doc;

        private int currentPage;

        public GhostPdfDocument(ILogger logger, string source)
        {
            this.logger = logger;
            this.source = source;
            this.doc = new GhostscriptRasterizer();
            this.doc.Open(
                this.source,
                GhostscriptVersionInfo.GetLastInstalledVersion(),
                true);
            this.currentPage = 1;
        } 

        public static int Dpi { get; set; } = 96;

        public int TotalPages => this.doc.PageCount;

        public void MoveToPage(int pageNumber)
        {
            if (pageNumber < 1 || pageNumber > this.TotalPages)
            {
                throw new ArgumentOutOfRangeException(nameof(pageNumber));
            }

            this.currentPage = pageNumber;
        }

        public void TakeSnapshot(string target)
        {
            try
            {
                using (var page = this.doc.GetPage(Dpi, Dpi, this.currentPage))
                {
                    page.Save(target, ImageFormat.Jpeg);
                }
            }
            catch (Exception e)
            {
                this.logger.Error(
                    e, 
                    "The following exception has occurred while saving {Page} of {File} to {Location}:",
                    this.currentPage,
                    this.source,
                    target);
                
                throw;
            }
        }

        protected override void DisposeCore()
        {
            this.doc.Dispose();
        }
    }
}