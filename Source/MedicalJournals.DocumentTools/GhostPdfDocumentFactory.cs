namespace MedicalJournals.DocumentTools
{
    using System;

    using Infrastructure;

    using Serilog;

    public class GhostPdfDocumentFactory : IPdfDocumentFactory
    {
        private readonly ILogger logger;

        public GhostPdfDocumentFactory(ILogger logger)
        {
            this.logger = logger;
        }

        public IPdfDocument Get(string source)
        {
            try
            {
                return new GhostPdfDocument(this.logger, source);
            }
            catch (Exception e)
            {
                const string Message = "The following exception has " +
                    "occurred while opening {PdfFile} to convert:";

                this.logger.Error(e, Message, source);

                throw;
            }
        }
    }
}