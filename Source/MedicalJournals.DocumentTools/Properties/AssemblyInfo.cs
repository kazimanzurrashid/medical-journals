﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("MedicalJournals.DocumentTools")]
[assembly: AssemblyProduct("MedicalJournals.DocumentTools")]
[assembly: CLSCompliant(true)]
[assembly: Guid("f175d0a6-3e17-4e28-b254-86cb0c1029b1")]
[assembly: NeutralResourcesLanguage("en-US")]
