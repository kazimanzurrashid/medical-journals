namespace MedicalJournals.Infrastructure
{
    using System;

    using Autofac;

    using DocumentTools;
    using ExtensionMethods;

    public class ServiceRegistrations : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            builder.Register<GhostPdfDocumentFactory>()
                .SingleInstance();
        }
    }
}